[https://univrelations.vcu.edu](VCU University Relations)<br> 
[https://digital.staging2.vcu.edu](Web System Library)


### *** This repository is PUBLIC ***

In order to collaborate across campus without cumbersome user management this repository will be publically accessible for cloning. All PULL requests will be reviewed and approved by University Relations Digital Team to ensure that secure information is not merged into this project.


Below you will find information on how to perform common tasks.<br>

First clone(download) this project to a directory on your local computer. Start a NODE.js command window and navigate to the directory that you downloaded the project to. Use 'cd' to 'Change Directory'<Br>
Example:

```Node.js
cd c:/downloads/library
```

In the root of the project directory, you can run the following commands:

### `npm install`

This will check the dependencies and install all packages required for the project. Run this the first time you download/clone the project to your local computer.

### `npm start`

Runs the website in a local development mode.<br>
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload when you make and save edits to any file in the project.<br>

### `npm test`

Launches the test runner in the interactive watch mode.<br>
Full testing suite is TBD.

### `npm run build`

Builds the website files for production to the `build` folder.<br>
It correctly bundles and optimizes the CSS file for the best performance.

The build folder will contain the minified CSS file ready to be deployed to TerminalFour!

This CSS will have vendor prefixes added automatically through [Autoprefixer](https://github.com/postcss/autoprefixer) so you don’t need to worry about it.

For example, this:

```css
.App {
  display: flex;
  flex-direction: row;
  align-items: center;
}
```

becomes this:

```css
.App {
  display: -webkit-box;
  display: -ms-flexbox;
  display: flex;
  -webkit-box-orient: horizontal;
  -webkit-box-direction: normal;
      -ms-flex-direction: row;
          flex-direction: row;
  -webkit-box-align: center;
      -ms-flex-align: center;
          align-items: center;
}
```
### `npm run images`

Performs image minification on all images in the 'image minification' folder and then saves the minified file(s) to the build folder under /images/image minification'. These files can then be uploaded to the T4 media library. Each time this script is run, the entire build/images/image minification folder will be deleted and then recreated with new minified images being saved.


// End of file