  	var compress_images = require('compress-images'), newfolder, themeName, source_directory, resized_directory, INPUT_path_to_your_images, OUTPUT_path, project_number, min_folder;
  	var Jimp = require('jimp');
  	var async = require("async");

 	INPUT_path_to_your_images = 'image-minification/**/*.{jpg,JPG,jpeg,JPEG,png,svg,gif}';
    OUTPUT_path = 'build/images/image-minification/';
    source_directory = 'image-minification/';
    resized_directory = 'build/images/image-minification/resized/**/*.{jpg,JPG,jpeg,JPEG,png,svg,gif}';

 	const fs = require('fs');
	const path = require('path');
	const archiver = require('archiver');
	const initialPath = path.join(OUTPUT_path, 'resized');
	const dir = path.join(OUTPUT_path, 'minified/');

	deleteFolderRecursive(OUTPUT_path);
	
// Add `finally()` to `Promise.prototype`
Promise.prototype.finally = function(onFinally) {
  return this.then(
    /* onFulfilled */
    res => Promise.resolve(onFinally()).then(() => res),
    /* onRejected */
    err => Promise.resolve(onFinally()).then(() => { throw err; })
  );
};

	if(process.argv.slice(2).length > 0){
		project_number = process.argv.slice(2).toString();
	} else {project_number = 'NO-PROJECT'}

	console.log('Working...');

	function deleteFolderRecursive(path) {
	  	if( fs.existsSync(path) ) {
		    fs.readdirSync(path).forEach(function(file,index){
				var curPath = path + "/" + file;
				if(fs.lstatSync(curPath).isDirectory()) { // recurse
					deleteFolderRecursive(curPath);
				} else { // delete file
					fs.unlinkSync(curPath);
				}
		    });
		    fs.rmdirSync(path);
	  	}
	};

	var getFolders = function(dir, done) {
		var results = [];
		results.push(dir);
		fs.readdir(dir, function(err, list) {
		if (err) return done(err);
		var pending = list.length;
		if (!pending) return done(null, results);
			list.forEach(function(file) {
				file = path.resolve(dir, file);
				fs.stat(file, function(err, stat) {
					if (stat && stat.isDirectory()) {
						results.push(file);
					  	getFolders(file, function(err, res) {
					    	results = results.concat(res);
					    	if (!--pending) done(null, results);
					  	});
					} else {					  
					  if (!--pending) done(null, results);
					}
				});
			});
		});
	};
	var zipitup = function(completed, file, callback) {
        		const archive = new archiver('zip', {
						zlib: { level: 9 } // Sets the compression level.
				});
				if(!fs.existsSync(path.join(initialPath, file.split('.')[0]))){fs.mkdirSync(path.join(initialPath, file.split('.')[0]), 0744)};
				const output = fs.createWriteStream(OUTPUT_path + file + '.zip');		
				
				// listen for all archive data to be written
				// 'close' event is fired only when a file descriptor is involved
				output.on('close', function() {
					//console.log(archive.pointer() + ' total bytes');
					console.log('Archiver has been finalized and the output file descriptor has closed.');
					console.log('Copying ZIP');
					setTimeout(function(){callback(this)}, 10000);	
				});
				output.on('finish', function() {
					
					
				});
				// This event is fired when the data source is drained no matter what was the data source.
				// It is not part of this library but rather from the NodeJS Stream API.
				// @see: https://nodejs.org/api/stream.html#stream_event_end
				output.on('end', function() {
				  console.log('Data has been drained and END is called.');
				 
				});

				// good practice to catch warnings (ie stat failures and other non-blocking errors)
				archive.on('warning', function(err) {
				  if (err.code === 'ENOENT') {
				    // log warning
				  } else {
				    // throw error
				    throw err;
				  }
				});	
				archive.pipe(output);

				//console.log('Removing temp resize folder');
				//deleteFolderRecursive('build/images/image minification/resized');
				//console.log('Adding new images to archive.zip');
				//console.log(path.join(dir, file.split('.')[0]));
				archive.directory(path.join(dir, file.split('.')[0]), false);
				archive.file(source_directory + file, { name: file });
				//console.log('Finalized: ' + file);
				archive.finalize();
	}

	getFolders(source_directory, function(err, results) {
  		if (err) throw err;	
		results.forEach(function(folder) {
  			console.log(folder)
			fs.readdir(folder, (err, files) => {
				//console.log(folder)
				//rootfile = process.cwd();
  				//newfolder = path.join(rootfile + '/build/images/'+ folder.replace(rootfile, ''));
  				//if (!fs.existsSync(newfolder)){
			    //	fs.mkdirSync(newfolder);
				//}
				//console.log('Reading files from input folder...');
				//console.log('Options: ', themeName);
			  	//console.log(files);
				if (err) throw err;
				for (const file of files) {
					//console.log(folder);
					//console.log(file);
					fs.stat(path.join(folder, file), function(err, stat) {
						if(err) console.log(err);
						if(stat.isDirectory()){
							return true;
						}
					});
					let square_file = '250x250_' + file;
					let thumb_file = '250x125' + file;
					let social_file = 'social_1200x627_' + file;
					let large_file = '1400w_' + file;
					let display_file = '1600x900_' + file;
					
					if(!fs.existsSync(OUTPUT_path)){fs.mkdirSync(OUTPUT_path, 0744)};
					if(!fs.existsSync(initialPath)){fs.mkdirSync(initialPath, 0744)};
					if(!fs.existsSync(dir)){fs.mkdirSync(dir, 0744)};
					Jimp.read(path.join(folder,file))
						.then(image =>{							
							image.clone()
								.resize(1400, Jimp.AUTO)
						      	.write(path.join(initialPath, file.split('.')[0], large_file), (err, image) => {
						      		if(err){console.error('Large: ' + err);}
						      		console.log('- Resized to large');
					      			image.clone()
										.cover(250, 250)
										.write(path.join(initialPath, file.split('.')[0], square_file), (err, image) => {
							      			if(err){console.error('Square: ' + err)};	
							      			console.log('- Resized to square');				      		
									  		image.clone()
												.cover(1600, 900)
									      		.write(path.join(initialPath, file.split('.')[0], display_file), (err, image) => {
									      			if(err){console.error('Display: ' + err)};
									      			console.log('- Resized to display');
									      			image.clone()
														.cover(1200, 627)
										      			.write(path.join(initialPath, file.split('.')[0], social_file), (err, image) => {
										      				if(err){console.error('Social: '+ err)};
										      				console.log('- Resized to Social');
										      				console.log('Compressing newly sized images...');
										      				var folder_path = path.normalize(initialPath + '/' + file.split('.')[0] + '/**/*.{jpg,JPG,jpeg,JPEG,png,svg,gif}');
										      				folder_path = folder_path.replace(/\\/g, '/');
										      				thisdir =  path.normalize(dir + '/' + file.split('.')[0] + '/');
										      				if(!fs.existsSync(thisdir)){fs.mkdirSync(thisdir, 0744)};
										      				console.log('Folder path for compressing: '+folder_path)
											  				compress_images(folder_path, thisdir, {compress_force: true, statistic: true, autoupdate: true}, false,
														        {jpg: {engine: 'mozjpeg', command: ['-quality', '60']}},
														        {png: {engine: 'pngquant', command: ['--quality=20-50']}},
														        {svg: {engine: 'svgo', command: '--multipass'}},
														        {gif: {engine: 'gifsicle', command: ['--colors', '64', '--use-col=web']}}, 
														        (err, completed) => {		
														        	zipitup(completed, file, function(){
														        		return false;
														        		// Once we finalize the shared team drive solution, we will deposit the files in a shared space
														        		PROJECT_PATH = path.join('U:/_PHOTO_/', project_number);
																		if(!fs.existsSync(PROJECT_PATH)){fs.mkdirSync(PROJECT_PATH, 0744)};
														        		fs.copyFile(OUTPUT_path + file + '.zip', path.join(PROJECT_PATH,file) + '.zip', (err) => {
														        			if (err) throw err;
													        			});
														        	});
														        	        	
															});
										      			});
								      			});
				      					});
				      			});					  		
					      		
					    })
					  	.then((image) => {
					  						  							  		
			  			})
					  	.catch(err => {
					    	console.error(err);
					    	return false;
						});
				}	 
			});
		});
	});
	

   	

    
 //-------------//
 // End of file //