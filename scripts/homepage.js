  	var compress_images = require('compress-images'), newfolder, themeName, source_directory, resized_directory, INPUT_path_to_your_images, OUTPUT_path, project_number, min_folder;
  	var Jimp = require('jimp');
  	var async = require("async");

 	INPUT_path_to_your_images = 'image-minification/**/*.{jpg,JPG,jpeg,JPEG,png,svg,gif}';
    OUTPUT_path = 'build/images/image-minification/';
    source_directory = 'image-minification/';
    resized_directory = 'build/images/image-minification/resized/**/*.{jpg,JPG,jpeg,JPEG,png,svg,gif}';

 	const fs = require('fs');
	const path = require('path');
	const archiver = require('archiver');
	const initialPath = path.join(OUTPUT_path, 'resized');
	const dir = path.join(OUTPUT_path, 'minified/');


	
// Add `finally()` to `Promise.prototype`
Promise.prototype.finally = function(onFinally) {
  return this.then(
    /* onFulfilled */
    res => Promise.resolve(onFinally()).then(() => res),
    /* onRejected */
    err => Promise.resolve(onFinally()).then(() => { throw err; })
  );
};

	if(process.argv.slice(2).length > 0){
		project_number = process.argv.slice(2).toString();
	} else {project_number = 'NO-PROJECT'}

	console.log('Working...');

	function deleteFolderRecursive(path, callback) {
	  	if( fs.existsSync(path) ) {
		    fs.readdirSync(path).forEach(function(file,index){
				var curPath = path + "/" + file;
				if(fs.lstatSync(curPath).isDirectory()) { // recurse
					deleteFolderRecursive(curPath);
				} else { // delete file
					fs.unlinkSync(curPath);
				}
		    });
		    fs.rmdirSync(path);
	  	}
	};

	var getFolders = function(dir, done) {
		var results = [];
		results.push(path.normalize(dir));
		fs.readdir(dir, function(err, list) {
		if (err) return done(err);
		var pending = list.length;
		if (!pending) return done(null, results);
			list.forEach(function(file) {
				file = path.resolve(dir, file);
				fs.stat(file, function(err, stat) {
					if (stat && stat.isDirectory()) {
						results.push(path.normalize(file));
					  	getFolders(file, function(err, res) {
					    	results = results.concat( res );
					    	if (!--pending) done(null, results);
					  	});
					} else {					  
					  if (!--pending) done(null, results);
					}
				});
			});
		});
	};
	
	deleteFolderRecursive(OUTPUT_path);

	getFolders(source_directory, function(err, results) {
  		if (err) throw err;	
		results.forEach(function(folder) {
  			console.log('Preparing images from folder: ' + folder)
			fs.readdir(folder, (err, files) => {
				//console.log(folder)
				//rootfile = process.cwd();
  				//newfolder = path.join(rootfile + '/build/images/'+ folder.replace(rootfile, ''));
  				//if (!fs.existsSync(newfolder)){
			    //	fs.mkdirSync(newfolder);
				//}
				//console.log('Reading files from input folder...');
				//console.log('Options: ', themeName);
			  	//console.log(files);
				if (err) throw err;
				for (const file of files) {
					//console.log(folder);
					//console.log(file);
					fs.stat(path.join(folder, file), function(err, stat) {
						if(err) console.log(err);return false;
						if(stat.isDirectory()){
							return false;
						}
					});
					console.log(file);
					//return false;

					const half_file = file;
					
					if(!fs.existsSync(OUTPUT_path)){fs.mkdirSync(OUTPUT_path, 0744)};
					if(!fs.existsSync(initialPath)){fs.mkdirSync(initialPath, 0744)};
					if(!fs.existsSync(dir)){fs.mkdirSync(dir, 0744)};
					var of = folder.split('image-minification');
				  		of = path.join(OUTPUT_path, 'resized', of[of.length-1]);
				  		if(!fs.existsSync(of)){fs.mkdirSync(of, 0744)};
					//console.log('Resizing - ' + file);
					Jimp.read(path.join(folder,file))
					/*
					.then(image => {	
						//console.log(social_file + '- Resized for Social');	
				  		image.clone()
			      		.scaleToFit(1200, Jimp.AUTO)
			      		.cover(1200, 627, Jimp.HORIZONTAL_ALIGN_MIDDLE | Jimp.VERTICAL_ALIGN_TOP)
			      		.write(path.join(initialPath, file.split('.')[0], social_file)); // save
				      	return image;
				  	})
				  	.then(image => {	
						//console.log(social_file + '- Resized for Social');	
				  		image.clone()
			      		.cover(1200, 627)
			      		.write(path.join(initialPath, file.split('.')[0], social_file2)); // save
				      	return image;
				  	})
				  	.then(image => {
				  		//console.log(large_file + '- Resized to the largest size of 2400px');	  	
			    		image.clone()
			      		.scaleToFit(2400, Jimp.AUTO)
			     	 	.write(path.join(initialPath, file.split('.')[0], large_file)); // save
				      	return image;
				  	})
				  	.then(image => {
				  		//console.log(large_file2 + '- Resized to the width of 1400px');	  	
			    		image.clone()
			      		.scaleToFit(1400, Jimp.AUTO)
			     	 	.write(path.join(initialPath, file.split('.')[0], large_file2)); // save
				      	return image;
				  	})
					.then(image => {
						//console.log(square_file + '- Resized to a 250x250px square cropped from the center');
						image.clone()
						.cover(250,250)
				      	.write(path.join(initialPath, file.split('.')[0], square_file)); // save
			    		return image;
				    })
				    .then(image => {
						//console.log(square_file2 + '- Resized to a 250x250px square cropped from the left');
						image.clone()
						.cover(250, 250, Jimp.HORIZONTAL_ALIGN_LEFT | Jimp.VERTICAL_ALIGN_TOP)
				      	.write(path.join(initialPath, file.split('.')[0], square_file2)); // save
			    		return image;
				    })
				    .then(image => {
						//console.log(square_file2 + '- Resized to a 250x250px square cropped from the left');
						image.clone()
						.cover(250, 250, Jimp.HORIZONTAL_ALIGN_RIGHT | Jimp.VERTICAL_ALIGN_TOP)
				      	.write(path.join(initialPath, file.split('.')[0], square_file3)); // save
			    		return image;
				    })
				  	.then(image => {	
				  		//console.log(thumb_file + '- Resized for max width of 250px');	  	
				    	image.clone()
				      	.scaleToFit(250, Jimp.AUTO)
				      	.write(path.join(initialPath, file.split('.')[0], thumb_file)); // save
				      	return image;
				  	}) */
				  	.then(image => {					  		
				  		console.log(path.join(of, half_file) + '- Resized');	  	
				    	image.clone()
				      	.scale(0.5)	
				      	.write(path.join(of, half_file)); // save
				      	return image;
				  	})			  	
				  	/* .then(image => {
				  		console.log('Compressing resized images: ' + file);
	      				var folder_path = path.normalize(initialPath + '/' + file.split('.')[0] + '/**\/*.{jpg,JPG,jpeg,JPEG,png,svg,gif}');
	      				folder_path = folder_path.replace(/\\/g, '/');
	      				thisdir =  path.normalize(dir + '/' + file.split('.')[0] + '/');
	      				if(!fs.existsSync(thisdir)){fs.mkdirSync(thisdir, 0744)};
	      				//console.log('Folder path for compressing: '+folder_path)
		  				compress_images(folder_path, thisdir, {compress_force: true, statistic: true, autoupdate: true}, false,
					        {jpg: {engine: 'mozjpeg', command: ['-quality', '60']}},
					        {png: {engine: 'pngquant', command: ['--quality=20-50']}},
					        {svg: {engine: 'svgo', command: '--multipass'}},
					        {gif: {engine: 'gifsicle', command: ['--colors', '64', '--use-col=web']}}, 
					        (err, completed) => {		
					        	
					        	        	
						});
				  	})
  					*/
				  	.catch(err => {
				    	console.error(err);
				    	return false;
					});
				}	 
			});
		});
	});
	

   	

    
 //-------------//
 // End of file //