  	var compress_images = require('compress-images'), image_quality, source_directory, OUTPUT_path;
    OUTPUT_path = 'build/images/';
    source_directory = 'image-minification/';

 	const fs = require('fs');
	const path = require('path');
	deleteFolderRecursive(OUTPUT_path);
	

	if(process.argv.slice(2).length > 0){
		image_quality = process.argv.slice(2);
	} else {image_quality = 60}

	console.log('Working...');

	function deleteFolderRecursive(path) {
	  	if( fs.existsSync(path) ) {
		    fs.readdirSync(path).forEach(function(file,index){
				var curPath = path + "/" + file;
				if(fs.lstatSync(curPath).isDirectory()) { // recurse
					deleteFolderRecursive(curPath);
				} else { // delete file
					fs.unlinkSync(curPath);
				}
		    });
		    fs.rmdirSync(path);
	  	}
	};

	var getFolders = function(dir, done) {
		var results = [];
		results.push(dir);
		fs.readdir(dir, function(err, list) {
		if (err) return done(err);
		var pending = list.length;
		if (!pending) return done(null, results);
			list.forEach(function(file) {
				file = path.resolve(dir, file);
				fs.stat(file, function(err, stat) {
					if (stat && stat.isDirectory()) {
						results.push(file);
					  	getFolders(file, function(err, res) {
					    	results = results.concat(res);
					    	if (!--pending) done(null, results);
					  	});
					} else {					  
					  if (!--pending) done(null, results);
					}
				});
			});
		});
	};

	// Resize and crop variations
		console.log('Compressing files to ' + image_quality + '% from input folder....');
		getFolders(source_directory, function(err, results) {
	  		if (err) throw err;	
	  		results.forEach(function(folder) {	
		  		newpath = path.dirname(require.main.filename);
		  		newpath = newpath.replace('\scripts', '');
		  		folder = folder.replace(newpath, '');
		  		FOLDER_FILES = path.normalize(folder + '/**/*.{jpg,JPG,jpeg,JPEG,png,svg,gif}');
		  		FOLDER_FILES = FOLDER_FILES.replace(/\\/g, '/');
		  		OUT = path.normalize(OUTPUT_path + '/' + folder + '/');
		  		console.log('IN ----- ' + FOLDER_FILES);
		  		console.log('OUT ----- ' + OUT);
		  		console.log('------------------------------');
		  		// return false;
	  			compress_images(FOLDER_FILES, OUT, {compress_force: false, statistic: true, autoupdate: true}, false,
			        {jpg: {engine: 'mozjpeg', command: [ '-quality', image_quality ]}},
			        {png: {engine: 'pngquant', command: ['--quality=20-50']}},
			        {svg: {engine: 'svgo', command: '--multipass'}},
			        {gif: {engine: 'gifsicle', command: ['--colors', '64', '--use-col=web']}}, function(){
				});			
	  		});			
		});				
	

   	

    
 //-------------//
 // End of file //