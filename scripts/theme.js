'use strict';

// Do this as the first thing so that any code reading it knows the right env.
process.env.BABEL_ENV = 'production';
process.env.NODE_ENV = 'production';

// Makes the script crash on unhandled rejections instead of silently
// ignoring them. In the future, promise rejections that are not handled will
// terminate the Node.js process with a non-zero exit code.
process.on('unhandledRejection', err => {
  throw err;
});



// Ensure environment variables are read.
require('../config/env');

const path = require('path');
const chalk = require('chalk');
const fs = require('fs-extra');
const webpack = require('webpack');
const config = require('../config/webpack.config.prod');
const paths = require('../config/paths');

const themeName = process.argv.slice(2);
console.log('myArgs: ', themeName);

 	// Create a copy of the VCUX stylesheets folder with the name provided when running the script

  fs.copySync(paths.appTheme+'/VCUX', paths.appTheme+'/'+themeName[0], {
    dereference: true,
    filter: file => file !== paths.appHtml,
  });

// Update the SCSS build file to use the new folder for building CSS

fs.readFile(paths.appTheme+'/main.scss', 'utf8', function (err,data) {
  if (err) {
    return console.log(err);
  }
  var result = data.replace(/VCUX/g, themeName[0]);

  fs.writeFile(paths.appTheme+'/main.scss', result, 'utf8', function (err) {
     if (err) return console.log(err);
  });
});


// END OF FILE