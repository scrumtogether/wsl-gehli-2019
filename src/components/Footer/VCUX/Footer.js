import React, { Component } from 'react';

class Footer extends Component {
constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      menu: [],
      section: props.page
    };
    this.getFooter = this.getFooter.bind(this);
  }

componentDidMount() {
    fetch(this.state.section, {cache: "no-store"} )
      .then( response => response.text() )
      .then(
        (response) => {
          this.setState({
            isLoaded: true,
            footer: this.getFooter(response)
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          console.log(error);
          this.setState({
            isLoaded: true, 
            error
          });
        }
      )
  }

  getFooter(footer){
    const rgx = /<footer([^]+.*?)<\/footer>/ig;
    const find = rgx.exec(footer);
    return find[0];
  }
  
  render() {
    const { error, isLoaded, footer } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return '';
    } else {
      return (
        <footer dangerouslySetInnerHTML={{__html: footer}}></footer>
      )
    }
  }
}

export default Footer;

// End of File //
// ----------  //