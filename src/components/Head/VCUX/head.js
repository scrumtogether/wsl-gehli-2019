import React, { Component } from 'react';

class Head extends Component {
constructor(props) {
    super(props); 
    this.state = {
      error: null,
      isLoaded: false,
      head: [],
      section: props.page
    };
    this.getHead = this.getHead.bind(this);
  }

componentDidMount() {
    fetch(this.state.section, {cache: "no-store"} )
      .then( response => response.text() )
      .then(
        (response) => {
          this.setState({
            isLoaded: true,
            head: this.getHead(response)
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          console.log(error);
          this.setState({
            isLoaded: true, 
            error
          });
        }
      )
  }

  getHead(head){
    const rgx = /<head>([^]+.*?)<\/head>/ig;
    const find = rgx.exec(head);
    console.log(find)
    return find[1] || head;
  }
  
  render() {
    const { error, isLoaded, head } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return '';
    } else {
      <head dangerouslySetInnerHTML={{__html: head}}></head>
    }
  }
}

export default Head;


// ----------- //
// End of fil�b�J