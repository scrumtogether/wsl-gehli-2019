import React, { Component } from 'react';

class Menu extends Component {
constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      menu: [],
      section: props.page
    };
    this.getMenu = this.getMenu.bind(this);
    this.resetDesktopMenu = this.resetDesktopMenu.bind(this);
  }

componentDidMount() {
    fetch(this.state.section, {cache: "no-store"} )
      .then( response => response.text() )
      .then(
        (response) => {
          this.setState({
            isLoaded: true,
            menu: this.getMenu(response)
          });
          this.resetDesktopMenu();
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          console.log(error);
          this.setState({
            isLoaded: true, 
            error
          });
        }
      )
  }

  getMenu(menu){
    const rgx = /<header>([^]+.*?)<\/header>/ig;
    const find = rgx.exec(menu);
    return find[1] || menu;
  }

  resetDesktopMenu() {
      var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
      if(width > 1024){
          var els = document.getElementsByClassName("is-expanded");
          while (els[0]) {
            let classes = String(els[0].className).replace("is-expanded", "is-collapsed");
            els[0].className = classes;
          }
      }
    }
  
  render() {
    const { error, isLoaded, menu } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return '';
    } else {
      return (
        <header dangerouslySetInnerHTML={{__html: menu}}></header>
      )
    }
    
}

}

export default Menu;


// ----------- //
// End of filb"��