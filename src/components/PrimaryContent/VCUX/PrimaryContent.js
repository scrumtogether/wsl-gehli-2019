import React, { Component } from 'react';

class PrimaryContent extends Component {
constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: [],
      cid: props.cid,
      url: props.url
    };
  }

  componentDidMount() {
    const link = this.state.url.replace("http://", "https://")
    fetch(link+"data-"+this.state.cid+".json", {cache: "no-store"} )
      .then( response => response.json() )
      .then(
        (response) => {
          this.setState({
            isLoaded: true,
            items: response.primaryContent
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          console.log(error);
          this.setState({
            isLoaded: true, 
            error
          });
        }
      )
  }

 render() {
    const { error, isLoaded, items } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return '';
    } else {
      return (
        <section aria-label={this.state.items[0].name} className={'content-primary ' + this.state.items[0].class}>
          <div className="l-wrapper">
            <div className="l-gutter">
              <div className="content-primary__content" dangerouslySetInnerHTML={{__html: decodeURIComponent(items[0].content) }} ></div>
            </div>
          </div>  
        </section>
      )
    }
  }
}

export default PrimaryContent;

// End of file //