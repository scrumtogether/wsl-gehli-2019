import React, { Component } from 'react';

class SecondaryContent extends Component {
constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      items: [],
      cid: props.cid,
      url: props.url
    };
  }

componentDidMount() {
    const link = this.state.url.replace("http://", "https://")
    fetch(link+"data-"+this.state.cid+".json" )
      .then( response => response.json() )
      .then(
        (response) => {
          this.setState({
            isLoaded: true,
            items: response.secondaryContent
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          console.log(error);
          this.setState({
            isLoaded: true, 
            error
          });
        }
      )
  }

 render() {
    const { error, isLoaded, items } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div class="react-loading">Loading...</div>;
    } else {
      return (
        <aside className="content-secondary">
          <div className="l-wrapper">
            <div className="l-gutter">
              <div className="content-secondary__content" dangerouslySetInnerHTML={{__html: decodeURI(items[0].content) }} >         
              </div>
              </div>
          </div>
        </aside>
      )
    }
  }
}



export default SecondaryContent;