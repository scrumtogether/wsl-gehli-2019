export const SiteURL = url => ({
	type: 'SITE_URL',
 	url: 'https://gehli2019.staging2.vcu.edu//'
})

export default SiteURL;

// End of file //
// ----------- //