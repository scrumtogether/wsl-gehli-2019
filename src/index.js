import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import SiteURL from './components/setup.js';
import Layout from './layouts/VCUX/layout';
import registerServiceWorker from './registerServiceWorker';
require('./stylesheets/main.scss');

// Setup state 
const store = createStore(SiteURL);

ReactDOM.render(<Provider store={store}><Layout /></Provider>, document.getElementById('react-page'));
registerServiceWorker();

//-------------//
// End of file //