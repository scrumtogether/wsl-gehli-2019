window.onload = function() {
	// register the key event handler 
	document.addEventListener('keyup', doc_keyUp, false);
	// Check for desktop size and remove all .is-expanded classes
    var width = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
    if (parseFloat(width) >= 1024) {
        var els = document.getElementsByClassName("is-expanded");
        while (els[0]) {
            let classes = String(els[0].className).replace("is-expanded", "is-collapsed");
            els[0].className = classes;
        }
    }
}

// Shortkeys
function doc_keyUp(e) {
    // Keycode 27 = 'ESC'
    if (e.keyCode == 27) {
    	return true;
        // remove all '.is-expanded, .show-menu, .show-search, .show-utility' classes
       	var els = document.querySelectorAll(".is-expanded, .show-menu, .show-search, .show-utility");
        while (els[0]) {
            let classes = String(els[0].className).replace("is-expanded", "is-collapsed");
            let classes = String(els[0].className).replace("show-menu", "");
            let classes = String(els[0].className).replace("show-utility", "");
            let classes = String(els[0].className).replace("show-search", "");
            els[0].className = classes;
        }
    }
}

/** Toggle shortcode tabs */
function toggleTabs(cid, sid, tab) {
  var els = document.getElementById("tab__sid-"+ sid).getElementsByClassName("is-active");
  while (els[0]){
    els[0].classList.remove("is-active");
  }
  document.getElementById('tab__' + cid).classList.add('is-active');
  tab.parentNode.parentNode.parentNode.classList.add('is-active');
} 

function scrollToElement(elementY, duration) { 
  var startingY = window.pageYOffset;
  var diff = elementY - startingY;
  var start;

  // Bootstrap our animation - it will get called right before next frame shall be rendered.
  window.requestAnimationFrame(function step(timestamp) {
    if (!start) start = timestamp;
    // Elapsed milliseconds since start of scrolling.
    var time = timestamp - start;
    // Get percent of completion in range [0, 1].
    var percent = Math.min(time / duration, 1);

    window.scrollTo(0, startingY + diff * percent);

    // Proceed with animation as long as we wanted it to.
    if (time < duration) {
      window.requestAnimationFrame(step);
    }
  })
}



//-------------//
// End of file //