import React, { Component } from 'react';
import SiteURL from '../../components/setup.js';
import Menu from '../../components/Menu/VCUX/menu.js';
import PageMain from '../VCUX/main.js';
import Footer from '../../components/Footer/VCUX/Footer.js';
import Swiper from 'swiper/dist/js/swiper.js';
//import Dashboard from '../../components/dashboard/dashboard.js';

class Layout extends Component {
    constructor(props) {
        //const store = createStore(SiteURL);
        super(props);
        this.state = {
            error: null,
            isLoaded: false,
            pages: [],
            section: SiteURL().url,
            root: SiteURL().url,
            page: null,
            pageID: 'react-page'
        };
        this.getLayout = this.getLayout.bind(this);
        this.callback = this.callback.bind(this);
        this.setupSwiper = this.setupSwiper.bind(this);
    }

    componentDidMount() {
        //console.log(this.store.getState());
        fetch(this.state.root + "/sitemap.json", { cache: "no-store" })
            .then(response => response.json())
            .then(
                (response) => {
                    this.setState({
                        isLoaded: true,
                        pages: response.sitemap,
                        page: this.getLayout(response.sitemap, this.state.root + '/')
                    });
                    document.getElementById(this.state.pageID).id = "ws-" + response.sitemap[0].id;
                    this.setState({pageID: 'ws-' + response.sitemap[0].id })
                },
                // Note: it's important to handle errors here
                // instead of a catch() block so that we don't swallow
                // exceptions from actual bugs in components.
                (error) => {
                    console.log(error);
                    this.setState({
                        isLoaded: true,
                        error
                    });
                }
            ).then(
            setTimeout(() => {
            	this.setupSwiper()
            }, 1000)
            )
        if (document.addEventListener)
            document.addEventListener('click', this.callback, false);
        else
            document.attachEvent('onclick', this.callback);
    }

    callback(e) {
        e = document.getElementById('nav-main').e || e;
        if (e.target.tagName !== 'A' || e.target.className === 'panel-title__link') return;
        e.preventDefault();

        // not sure the correct way to do this, but this forces new layout render
        this.setState({
            page: this.getLayout(this.state.pages, this.state.root + e.target.pathname),
            section: this.state.root + e.target.pathname,
            isLoaded: false
        });
        if(this.state.page.length > 0) {
            document.getElementById(this.state.pageID).id = "ws-" + this.state.page[0].id;
            this.setState({pageID: 'ws-' + this.state.page[0].id});
        }
        this.setState({ isLoaded: true });
        this.setupSwiper();
    }

    getLayout(sitemap, url) {
        return sitemap.filter(sitemap => sitemap.url === url);
    }

    setupSwiper() {
        new Swiper("#swiper-309167", {
            watchOverflow: true,
            direction: "horizontal",
            loop: false,
            autoHeight: true,
            slidesPerView: 1,
            spaceBetween: 10,
            pagination: {el: ".swiper-pagination", clickable: true},
            navigation: {nextEl: ".swiper-button-next", prevEl: ".swiper-button-prev"},
        })
        new Swiper('#swiper-284183', {
            // Optional parameters
            direction: 'horizontal',
            loop: true,
            autoHeight: true,

            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },
        })
        new Swiper('#swiper-284025', {
            // Optional parameters
            direction: 'horizontal',
            loop: true,
            autoHeight: true,
            slidesPerView: 3,
            spaceBetween: 16,
            breakpoints: {
                // when window width is <= 768px
                768: {
                    slidesPerView: 1,
                    spaceBetween: 0
                }
            },

            // If we need pagination
            pagination: {
                el: '.swiper-pagination',
                clickable: true,
            },

            // Navigation arrows
            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

        })

    }

    render() {
        const { error, isLoaded } = this.state;
        if (error) {
            return <div > Error: { error.message } < /div>;
        } else if (!isLoaded) {
            return <div className = "react-loading" > Loading... < /div>;
        } else {
            return ( <
                React.Fragment >
                <
                ul className = "skip-menu skip-menu__off-screen-ul"
                role = "complementary"
                aria-label = "Skip links menu" >
                <
                li className = "skip-menu__skip-item" >
                <
                a href = "#page-main"
                tabIndex = "1"
                className = "skip-menu__skip-link" > Skip to content < /a> <
                /li> <
                li className = "skip-link__skip-item" >
                <
                a href = "#page-header__menu"
                tabIndex = "2"
                className = "skip-menu__skip-link" > Skip to Navigation < /a> <
                /li> <
                li className = "skip-menu__skip-item" >
                <
                a href = "#page-footer"
                tabIndex = "3"
                className = "skip-menu__skip-link" > Skip to footer < /a> <
                /li> <
                /ul> <
                Menu page = { this.state.section }
                /> <
                PageMain page = { this.state.section }
                /> <
                Footer page = { this.state.section }
                /> <
                /React.Fragment>
            )
        }
    }
}

export default Layout;

// ----------- //
// En��uE�oG��/