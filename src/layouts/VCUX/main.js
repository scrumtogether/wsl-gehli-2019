import React, { Component } from 'react';
import SiteURL from '../../components/setup.js';

class PageMain extends Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      url: props.page
    };
    this.getMain = this.getMain.bind(this);
  }

  componentDidMount() {
    //const link = this.state.url.replace("http://", "https://")
    fetch(this.state.url, {cache: "no-store"} )
      .then( response => response.text() )
      .then(
        (response) => {
          this.setState({
            isLoaded: true,
            pageMain: this.getMain(response)
          });
        },
        // Note: it's important to handle errors here
        // instead of a catch() block so that we don't swallow
        // exceptions from actual bugs in components.
        (error) => {
          console.log(error);
          this.setState({
            isLoaded: true, 
            error
          });
        }
      )
  }
  getMain(main){
    const rgx = /<main id="page-main" class="page-main">([^]+.*?)<\/main>/ig;
    const find = rgx.exec(main);
    main = find[1];
    var rgxSRC = /(src="([^].*?)")/ig,
        links = rgxSRC.exec(main),
        src = [];
        
    while (links != null) {
      src.push(String(links[2]));
      links = rgxSRC.exec(main);
    }
    for (var i=0;i<src.length;i++){
      if(src[i][0] !== 'h'){
        main = main.replace('src="' + src[i] + '"', 'src="'+ SiteURL().url + src[i] + '"');
      }
    }
    // upate CSS URL with absolute URL
    var rgxURL = /(url\(([^].*?)\))/ig,
        URLlinks = rgxURL.exec(main),
        URL = [];
        
    while (URLlinks != null) {
      URL.push(String(URLlinks[2]));
      URLlinks = rgxURL.exec(main);
    }
    for (i=0;i<URL.length;i++){
      main = main.replace('url(' + URL[i] + ')', 'url('+ SiteURL().url + URL[i] + ')');
    }
    return main;
  }

  render() {
    const { error, isLoaded, pageMain } = this.state;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
     return <div className="react-loading">Loading...</div>;
    } else {
      return (
        <main id="page-main" className="page-main" dangerouslySetInnerHTML={{__html: pageMain}}></main>
      )
    }
  }
}

export default PageMain;

// ----------- //
// End of file //