try {
    importPackage(com.terminalfour.list);
    importPackage(com.terminalfour.template);
  importClass(org.apache.commons.io.IOUtils);
  importClass(com.terminalfour.media.MediaManager);
    importClass(com.terminalfour.publish.PathBuilder);
  importClass(com.terminalfour.publish.utils.TreeTraversalUtils);
  importClass(com.terminalfour.content.Content);
  importClass(com.terminalfour.content.ContentHelper);
  importClass(com.terminalfour.sitemanager.cache.CachedContent);
    importClass(com.terminalfour.sitemanager.cache.CachedSection);
    importClass(com.terminalfour.list.PredefinedList);
    importClass(com.terminalfour.list.PredefinedListMetaData);
    importClass(com.terminalfour.list.PredefinedListManagerImpl);
    var T4_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 168778, language).getMedia())));
    var VCU_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 189394, language).getMedia())));
    var UR_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 265744, language).getMedia())));    
    var pubCache = publishCache.getMicroSite(section); 
    var baseHREF = pubCache.getBaseHref();
    if(isPreview){baseHREF = ''}
    
  // Setup objects and variables
    var oCH = new ContentHierarchy(),
      oCM = com.terminalfour.spring.ApplicationContextProvider.getBean(com.terminalfour.content.IContentManager),
        rootSection = TreeTraversalUtils.findSection(publishCache.getChannel(), section, pubCache.getRootSectionID(), language),
        childs = rootSection.getChildren(publishCache.channel, true);
  
    childs.sort(function (a, b) { // Sort the menu sections in sequence
      return a.getPrintSequence() - b.getPrintSequence();
    });
  
  document.write('{"sitemap":[');
    // Add homepage to array first  
      contentList = rootSection.getContentIds(),
            LINK = baseHREF + PathBuilder.getLink(dbStatement, rootSection, publishCache, language, isPreview).getLink(),
          cList = '[';
            
            for(d = 0;d<contentList.length;d++){
              cList = cList + '{"content":' + contentList[d] + ', "type":' + oCM.get(Number(contentList[d] ),"en").getContentTypeID() + '}'; 
              if(d+1 != contentList.length){
                cList = cList + ',';            
              }
            }
        cList = cList + ']';
  
        document.write('{');
            document.write('"id":' + rootSection.getID() + ',');
            document.write('"url": "' + LINK + '",');
            document.write('"content": ' + cList.toString());
            document.write('},');
  
   
      for(i=0;i<childs.length;i++){   
      
    sectionID = childs[i].getID(),
        sectionURL = childs[i].getOutputURI('en'),
        children = childs[i].getChildren(publishCache.channel, true),
        link = PathBuilder.getLink(dbStatement, childs[i], publishCache, language, isPreview),
        findLink = /(<a href="([^].*?)")/i,
        URL = findLink.exec(String(link));
        
        children.sort(function (a, b) { // Sort the menu sections in sequence
          return a.getPrintSequence() - b.getPrintSequence();
        });
    
      //contentList = childs[i].getContentIds();
      contentListOrder = childs[i].getContentsAndSequences().entrySet().iterator();
      var contentList = [];

        while (contentListOrder.hasNext()) {
            var con = contentListOrder.next();
            contentList.push({"cid": Number(con.getKey()), "seq": Number(con.getValue()) });
            //document.write('*************' + ok.getKey() + ' -> ' + ok.getValue() + '************\n');
        }
    
      contentList.sort(function (a, b) { // Order the content
          return a.seq - b.seq;
        });
    
      LINK = baseHREF + PathBuilder.getLink(dbStatement, childs[i], publishCache, language, isPreview).getLink(),
        cList = '[';
          
            for(d = 0;d<contentList.length;d++){
              //document.write('******' + contentList[d]['cid'] + '*******');
              cList = cList + '{"content":' + contentList[d]['cid'] + ', "type":' + oCM.get(Number(contentList[d]['cid'] ),"en").getContentTypeID() + '}'; 

              if(d+1 != contentList.length){
                cList = cList + ',';            
              }
            }
          cList = cList + ']';
          
            document.write('{');
            document.write('"id":' + childs[i].getID() + ',');
            document.write('"url": "' + LINK + '",');
            document.write('"content": ' + cList.toString());
            document.write('}');
      
            if(i+1 != childs.length || children.length > 0){
              document.write(',');            
            }
    
      
            for(a=0;a<children.length;a++){
              //link = PathBuilder.getLink(dbStatement, children[a], publishCache, language, isPreview),
               // findLink = /(<a href="([^].*?)")/i,
               // URL2 = findLink.exec(String(link)),
                contentList = children[a].getContentIds(),
                LINK = baseHREF + PathBuilder.getLink(dbStatement, children[a], publishCache, language, isPreview).getLink(),
                cList = '[';

                for(d = 0;d<contentList.length;d++){
                  //cList = cList + contentList[d]
                  cList = cList + '{"content":' + contentList[d] + ', "type":' + oCM.get(Number(contentList[d] ),"en").getContentTypeID() + '}'; 

                  if(d+1 != contentList.length){
                    cList = cList + ',';            
                  }
                }
                cList = cList + ']';

                document.write('{');
                document.write('"id":' + children[a].getID() + ',');
                document.write('"url": "' + LINK + '",');
                document.write('"content": ' + cList.toString());
                document.write('}');

                if(a+1 < children.length || i+1 < childs.length){
                  document.write(',');            
                }


              //third level??
             // third = c[a].getChildren(publishCache.channel, true);
             // third.sort(function (a, b) { // Sort the menu sections in sequence
              //  return a.getPrintSequence() - b.getPrintSequence();
             // });

            };
        
    };
    
 
  document.write(']}');
      
      
  
  
    } catch (e) {
    if (e instanceof SyntaxError) {
        document.write('<strong>Syntax Error:</strong> ' + e.message);
    } else {
      document.write('<strong>T4 Error:</strong> ' + e.message);
  }
}
