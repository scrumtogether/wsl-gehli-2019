try {
    importPackage(com.terminalfour.list);
    importPackage(com.terminalfour.template);
  importClass(org.apache.commons.io.IOUtils);
  importClass(com.terminalfour.media.MediaManager);
    importClass(com.terminalfour.publish.PathBuilder);
  importClass(com.terminalfour.publish.utils.TreeTraversalUtils);
  importClass(com.terminalfour.content.Content);
  importClass(com.terminalfour.content.ContentHelper);
  importClass(com.terminalfour.sitemanager.cache.CachedContent);
    importClass(com.terminalfour.sitemanager.cache.CachedSection);
    importClass(com.terminalfour.list.PredefinedList);
    importClass(com.terminalfour.list.PredefinedListMetaData);
    importClass(com.terminalfour.list.PredefinedListManagerImpl);
    var T4_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 168778, language).getMedia()))),
      VCU_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 189394, language).getMedia()))),
      UR_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 265744, language).getMedia()))),   
      pubCache = publishCache.getChannel(),
      microsite = T4Utils.publishCache.microsite.parentChannel,
        rootPath = T4Utils.publishCache.microsite.baseHref;
    
    // Get variables and pieces
    var subnav = T4Utils.getSectionInfo.getChildren(section, false);
    var rootLink = T4Utils.brokerUtils.processT4Tag('<t4 type="navigation" name="Plugin - Homepage Path" id="181" />');
    var mediaManager = MediaManager.getManager();
    
    //var mediaID = content.get("Background image").getID();
    //var mediapath = T4Utils.brokerUtils.processT4Tag('<t4 type="media" id="' + mediaID + '" formatter="path/*"/>');
    var oCH = new ContentHierarchy();
    var oCM = com.terminalfour.spring.ApplicationContextProvider.getBean(com.terminalfour.content.IContentManager);
    var rootSectionID = publishCache.getMicroSiteFromChild(section).getRootSectionID();
    var rootSection = TreeTraversalUtils.findSection(publishCache.getChannel(), section, rootSectionID, language);
    var c = rootSection.getChildren(publishCache.channel, false);
    var parentURL = content.get('Parent or department URL');
    var parentName = content.get('Parent or department');
    var addr1 = content.get('Address 1');
    var addr2 = content.get('Address 2');
    var addr3 = content.get('Address 3');
    var tel = content.get('Telephone');
  var telRefined = tel.toString().replaceAll("[^0-9]", "");
    var footerCenterContentWrite = ''; 
    var email = content.get('Email');
  
  
  if(content.get('Center column content') != ''){
    var footerCenter = String(content.get('Center column content')).replace('<t4 type="sslink" sslink_id="','').replace('"/>','');
      link = com.terminalfour.navigation.ServerSideLinkManager.getManager().getLink(dbStatement.getConnection(), Number(footerCenter), section.getID(), content.getID(), language),
        sectionID = link.getToSectionID(),
        footerCenterSection = TreeTraversalUtils.findSection(publishCache.getChannel(), section, sectionID, language),
          footerCenterContent = footerCenterSection.getContent();

    
            sortedCenterContent = new Array();
        for(var a=0;a<footerCenterContent.length;a++){
          var s = footerCenterContent[a].toString().split('sequence: ');
          s2 = s[1].split('Content');
          s3 = Number(s2[0]);
          var id = footerCenterContent[a].toString().split('id: ');
          id = id[1].split('- en:');
          id = Number(id[0]);
          sortedCenterContent.push({'seq': s3, 'id': id});
        }

        sortedCenterContent.sort(function (a, b) {
          return a.seq - b.seq;
        });


        
        // Write out content
      footerCenterContentWrite = '<div class="footer-contact__center">';
        for(b=0;b<sortedCenterContent.length;b++){
          var mo = oCM.get(sortedCenterContent[b]['id'],"en");
          footerCenterContentWrite = footerCenterContentWrite + doShortcodes(convertTags(mo.get('Content'), mo.getID(), sectionID));
        }
       footerCenterContentWrite = footerCenterContentWrite + '</div>';
    
  }
  
  
    
    if(addr1 != ''){addr1 = addr1+'<br>'}
    if(addr2 != ''){addr2 = addr2+'<br>'}
    if(addr3 != ''){addr3 = addr3+'<br>'}
  if(tel != ''){tel = '<li><a href="tel:'+telRefined+'" title="Contact us by telephone">'+tel+'</a></li>'}
  if(email != ''){email = '<li><a href="mailto:'+email+'" title="Contact us by email">'+email+'</a></li>'}
  
    html = [
        '<div class="footer-contact">',
          '<ul class="footer-contact__left">',
            '<li><a href="//vcu.edu" title="Virginia Commonwealth University" aria-label="Visit vcu.edu">Virginia Commonwealth University</a></li>',
            '<li><a href="' + parentURL + '" title="' + parentName + '">' + parentName + '</a></li>',
            '<li>',
              '<address>',
                addr1,
                addr2,
                addr3,
              '</address>',
            '</li>',
            tel,
            email,      
          '</ul>',
        footerCenterContentWrite,
      '<div class="footer-contact__right">',
        '<ul class="footer-contact__right-social">',
              '<li><a href="//facebook.com/virginiacommonwealthuniversity"><i class="fab fa-facebook-f"><span class="sr-only">Visit Facebook for VCU</span></i></a></li>',
              '<li><a href="//twitter.com/VCU"><i class="fab fa-twitter"><span class="sr-only">Visit Twitter for VCU</span></i></a></li>',
              '<li><a href="//instagram.com/vcu"><i class="fab fa-instagram"><span class="sr-only">Visit Instagram for VCU</span></i></a></li>',
            '<li><a href="//snapchat.com/add/vcu"><i class="fab fa-snapchat-ghost"><span class="sr-only">Visit Snapchat for VCU</span></i></a></li>',
            '</ul>',
      '</div>',
      '</div>',
      '<div class="footer-contact__standards">',
        '<ul class="footer-contact__standards-list">',
          '<li><a title="VCU privacy statement" href="//www.vcu.edu/vcu/privacy-statement.html" target="_blank">Privacy</a></li>',
          '<li><a title="Accessibility at VCU" href="http://accessibility.vcu.edu/" target="_blank">Accessibility</a></li>',
          '<li><a title="Contact the VCU webmaster" href="mailto:webmaster@vcu.edu" target="_blank">Webmaster</a></li>',
          '<li><a href="http://text.vcu.edu:8080/tt/referrer">View text version</a></li>',
          '<li>Created by&nbsp;<a href="http://www.univrelations.vcu.edu/" title="VCU Univeristy Relations">VCU University Relations</a></li>',    
          '<li>{{direct_edit_link}}: {{updated_date}}</li>',
        '</ul>',
      '</div>'
    ].join('\n');
  

} catch (e) {
    if (e instanceof SyntaxError) {
        document.write('<strong>Syntax Error:</strong> ' + e.message);
    } else {
      document.write('<strong>T4 Error:</strong> ' + e.message);
  }