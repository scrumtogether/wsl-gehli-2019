importClass(com.terminalfour.content.ContentManagerImpl);

function cleanNews(a){
  rgx = /(<img([^].*?)>)/ig,
  images = rgx.exec(a);
  allImgs = [];
  while (images != null) {
  	allImgs.push(String(images[0]));
	images = rgx.exec(a);
  }
  if(allImgs.length > 0){
    for (var i=0;i<allImgs.length;i++){
      url = 'src="http://news.vcu.edu';
      img1 = String(allImgs[i]).split('src="'); 
      url = String(img1[0]) + url + String(img1[1]);
      a = a.replace(allImgs[i], url);
    }
  }
  return a;
}


function getFullTextLink(sid,contentName,sectionObject) {
	//only do this once; declare these classes globally
   	/*if ( typeof(this.builder) == 'undefined')   {
            this.builder = com.terminalfour.publish.PathBuilder;
            this.tree = com.terminalfour.publish.utils.TreeTraversalUtils;
            this.builderutils = new com.terminalfour.publish.utils.PathBuilderUtils();
   	}*/
    try {
      	builderutils = new com.terminalfour.publish.utils.PathBuilderUtils();
        //retrieve the cachedSection
        sec = TreeTraversalUtils.findSection(publishCache.getChannel(), section, sid, language);		
        //get it's directory path
      	dir = PathBuilder.getLink (dbStatement, sec, publishCache, language, isPreview).getLink();
        //dir = builder.getDirectory( sec, publishCache,language);
        //escape spaces and special characters from the content name
        ftextname = builderutils.generateFilenameFromElementValue(publishCache, contentName, null, 'html');
        //remove the server directory path and combine the directory with the text name
        return dir.replace(SVR_FILE_PATH,"")+ftextname;
    } catch(e) {
    	document.write(e);
    	return '';
    }
}

function convertTags(txt, cid, sid){
  var find = /(<t4([^].*?)>)/ig,
  	  match = find.exec(txt),
      t4tags = [];
  while (match != null) {
    t4tags.push(String(match[0]));
    match = find.exec(txt);
  } // END while
  if(t4tags.length == 0){
    return txt;
  } else {
    
    
    for (var i=0;i<t4tags.length;i++){
      var findType = /(type="([^].*?)")/i,
  	  	  type = findType.exec(t4tags[i]);
      
      if(type[2] == 'sslink'){
        var findID = /(sslink_id="([^].*?)")/i,
  	    sslID = findID.exec(t4tags[i]);
        
        link = com.terminalfour.navigation.ServerSideLinkManager.getManager().getLink(dbStatement.getConnection(), Number(sslID[2]), Number(sid), Number(cid), language);
        
        var sectionID = link.getToSectionID();
        var name = link.getLinkText();
        var target = TreeTraversalUtils.findSection(publishCache.getChannel(), section, sectionID, language);
        
        if(name == ''){
          var link = PathBuilder.getLink(dbStatement, target, publishCache, language, isPreview);
          txt = String(txt).replace(t4tags[i], link);

        } else {
          var link = PathBuilder.getLink(dbStatement, target, publishCache, language, isPreview);
          var href = /(href="([^].*?)")/i;
          var url = href.exec(link);
          txt = String(txt).replace(t4tags[i], '<a href="'+url[2]+'">'+name+'</a>');
        } // END if there is a name
        
      } else if (type[2] == 'media'){
            var mediaID = /(id="([^].*?)")/i,
  	  	  	mID = mediaID.exec(t4tags[i]);
        	var formatter = /(formatter="([^].*?)")/i,
  	  	  	f = formatter.exec(t4tags[i]);
        	if(f[2] == 'application/*'){
            	var fMedia = contentApplicationConvert(mID[2]);
              	txt = String(txt).replace(t4tags[i], fMedia);
            } else {
            	var image = contentImageConvert(mID[2]);
				txt = String(txt).replace(t4tags[i], image);
            }
      }
    }// END for loop
  

  } // end null check
  return txt;

}

	// Scan the content and look for shortcodes
function doShortcodes(c) {
    //Find navigation shortcodes
    var rgx = /({nav_([^].*?)})/ig,
    	codes = rgx.exec(c),
        tags = [];
    while (codes != null) {
    	tags.push(String(codes[0]));
    	codes = rgx.exec(c);
    }
  	if(tags.length == 0){
    	return doTabs(c);
  	} else {
      
      for (var i=0;i<tags.length;i++){
        navid = String(tags[i]).replace('{','').replace('}','').split('_');
        navid = navid[1];
        var navObject = T4Utils.brokerUtils.processT4Tag('<t4 type="navigation" name="Get this nav content" id="'+String(navid)+'" />');
        c = c.replace('<p>'+String(tags[i])+'</p>', doShortcodes(navObject));
      	c = c.replace(String(tags[i]), doShortcodes(navObject));
      }
    }
 	return doTabs(c);
  }
  
function doTabs(c) {
    	var rgx = /({tabbed_([^].*?)})/ig,
    	codes = rgx.exec(c),
        tags = [];
    while (codes != null) {
    	tags.push(String(codes[0]));
    	codes = rgx.exec(c);
    }
  	if(tags.length == 0){
    	return doFlexsliders(c);
  	} else {      
      for (var i=0;i<tags.length;i++){
        sectionid = String(tags[i]).replace('{','').replace('}','').split('_');
        sectionid = sectionid[1]; 
        var targetSection = TreeTraversalUtils.findSection(publishCache.getChannel(), section, sectionid, language);
        var tabs = targetSection.getContent();
        var tab = '<ul class="nav nav-tabs">';

        //Build tabs   
        sortedContent = new Array();
        for(var a=0;a<tabs.length;a++){
          var s = tabs[a].toString().split('sequence: ');
          s2 = s[1].split('Content');
          s3 = Number(s2[0]);
          var id = tabs[a].toString().split('id: ');
          id = id[1].split('- en:');
          id = Number(id[0]);
          sortedContent.push({'seq': s3, 'id': id});
        }

        sortedContent.sort(function (a, b) {
          return a.seq - b.seq;
        });
		var active;   

        
        // Write out each tab 
        for(b=0;b<sortedContent.length;b++){
          if(b==0){active = ' class="active"'}else{active='';}
          var mo = oCM.get(sortedContent[b]['id'],"en");
          tab = tab + '<li'+active+'><a data-toggle="tab" href="#tabs-'+mo.getID()+'">' + mo.get('Name') + '</a></li>';
        }
        tab = tab + '</ul><div class="tab-content">'; 
    
        for(b=0;b<sortedContent.length;b++){
        	var mo = oCM.get(sortedContent[b]['id'],"en");
      		var sw = new java.io.StringWriter();
          	var t4w = new com.terminalfour.utils.T4StreamWriter(sw);          
			new com.terminalfour.publish.ContentPublisher().write(t4w, dbStatement, publishCache, section, mo, 'text/html', isPreview);
        	//var tid = mo.getTemplateID();
			//var tempManager = com.terminalfour.template.TemplateManager.getManager();
			//var formatter ='text/html';
			//var format = tempManager.getFormat(dbStatement,tid,formatter); //bring back an object of this template
			//var formatString = format.getFormatting(); //render the content on that object
			//var tabContent = com.terminalfour.publish.utils.BrokerUtils.processT4Tags(dbStatement, publishCache, section, mo, language, isPreview, formatString);

          	if(b==0){active = ' in active'}else{active='';}
          		var cont = mo.get('Content');
          		var shortcodeContent = convertTags(cont, sortedContent[b]['id'], sectionid);
          		shortcodeContent = doShortcodes(String(shortcodeContent));
          		tab = tab + '<div id="tabs-'+mo.getID()+'" class="tab-pane fade'+active+'">' + shortcodeContent + '</div>';
        	}                                 
      	 tab = tab + '</div>';
     	 c = c.replace(String(tags[i]), tab);
      }
    } 
 	return doFlexsliders(c);
}

function doFlexsliders(c) {
    var rgx = /({flexslider_([^].*?)})/ig,
    	codes = rgx.exec(c),
        tags = [];
    while (codes != null) {
    	tags.push(String(codes[0]));
    	codes = rgx.exec(c);
    }
  	if(tags.length == 0){
    	return c;
  	} else {     	
		// Create slides
		for (var i=0;i<tags.length;i++){
          	var slider = '<div class="gallery-shortcode-flexslider"><ul class="gallery-shortcode-flexslider-slides">';
        	var FSid = String(tags[i]).replace('{','').replace('}','').split('_');
          	FSid = FSid[1];
          	media = oCH.getContent(dbStatement, FSid, 'en');
          	for(a=0;a<media.length;a++){
            	var p = mediaManager.get(dbStatement, media[a], "en");
            		fileID = p.getID(),
            		desc = p.getDescription(),
            		file = '<t4 type="media" id="' + fileID + '" formatter="path/*" />',
            		img =  com.terminalfour.publish.utils.BrokerUtils.processT4Tags(dbStatement, publishCache, section, content, language, isPreview, file);
            	slider = slider + "<li class='gallery-shortcode-flexslider-slide'><img src='"+img+"' alt='"+desc+"'><p>"+desc+"</p></li>";
          	}
        	slider = slider + '</ul></div>';
          	c = c.replace('<p>'+String(tags[i])+'</p>', slider);
			c = c.replace(String(tags[i]), slider);
      	}
		
    }
  	return c;
  }


	function contentImagePath(input) {
      var mediaTag = '<t4 type="media" id="' + input.getID() + '" formatter="path/*"/>';
      return com.terminalfour.publish.utils.BrokerUtils.processT4Tags(dbStatement, publishCache, section, content, language, isPreview, mediaTag);
    }
  
  	function contentImage(input) {
      var mediaTag = '<t4 type="media" id="' + input.getID() + '" formatter="image/*"/>';
      return com.terminalfour.publish.utils.BrokerUtils.processT4Tags(dbStatement, publishCache, section, content, language, isPreview, mediaTag);
    }

  	function contentImageConvert(input) {
      var mediaTag = '<t4 type="media" id="' + input + '" formatter="image/*"/>';
      return com.terminalfour.publish.utils.BrokerUtils.processT4Tags(dbStatement, publishCache, section, content, language, isPreview, mediaTag);
    }
	function contentApplicationConvert(input) {
      var mediaTag = '<t4 type="media" mediaattributes="{}" id="' + input + '" formatter="application/*"/>';
      return com.terminalfour.publish.utils.BrokerUtils.processT4Tags(dbStatement, publishCache, section, content, language, isPreview, mediaTag);
    }

function contentImageTag(input) {
      var mediaTag = input;
      return com.terminalfour.publish.utils.BrokerUtils.processT4Tags(dbStatement, publishCache, section, content, language, isPreview, mediaTag);
    }

function addDirectEditLinkToFooter(siteFooter, lastUpdatedDate) {
  	directEditLink =  T4Utils.brokerUtils.processT4Tag('<t4 type="edit-page" action="direct-edit" text="Updated" />');
  	//directEditLink2 = String(directEditLink).split('<a');
  	//directEditLink = '<a aria-role="link" aria-label="Direct edit this page" ' + directEditLink2[0];
    insertPointRgx = /\{\{direct_edit_link\}\}/g;
    if (insertPointRgx.test(siteFooter)) return addUpdatedDateToFooter(siteFooter.replace(insertPointRgx, directEditLink), lastUpdatedDate);
  return siteFooter;
}

function addUpdatedDateToFooter(siteFooter, lastUpdatedDate) {
  	//var lastUpdatedDate = T4Utils.brokerUtils.generateT4Tag({type:'navigation',id:42});
    insertPointRgx = /\{\{updated_date\}\}/g;
    if (insertPointRgx.test(siteFooter)) return siteFooter.replace(insertPointRgx, lastUpdatedDate);
    return siteFooter;
}

function contentImageAlt(input) {
      var mediaTag = '<t4 type="media" id="' + input.getID() + '" formatter="image/description"/>';
      return T4Utils.brokerUtils.processT4Tag(mediaTag);
}

/*
var lastUpdatedDate = T4Utils.brokerUtils.generateT4Tag({type:'navigation',id:42}),
    directEditLink =  T4Utils.brokerUtils.processT4Tag('<t4 type="edit-page" action="direct-edit" text="Updated" />'),
    formattedFooter = addDirectEditLinkToFooter(siteFooter, directEditLink);
		formattedFooter = addUpdatedDateToFooter(formattedFooter, lastUpdatedDate)
*/
// Reorder content
 function reOrder(a,b){
      	var s = a.toString().split('sequence: ');
      	s = s[1].split('content');
      	s = Number(s[0]);
    var t = b.toString().split('sequence: ');
      	t = t[1].split('content');
      	t = Number(t[0]);
  	return s - t;
  
  }


var status = new Array();
status[0] = "Approved";
status[1] = "Pending";

var weekday = new Array();
weekday[0] = "Sunday";
weekday[1] = "Monday";
weekday[2] = "Tuesday";
weekday[3] = "Wednesday";
weekday[4] = "Thursday";
weekday[5] = "Friday";
weekday[6] = "Saturday";

var APweekday = new Array();
APweekday[0] = "Sun.";
APweekday[1] = "Mon.";
APweekday[2] = "Tue.";
APweekday[3] = "Wed.";
APweekday[4] = "Thur.";
APweekday[5] = "Fri.";
APweekday[6] = "Sat.";

var month = new Array();
month[0] = "January";
month[1] = "February";
month[2] = "March";
month[3] = "April";
month[4] = "May";
month[5] = "June";
month[6] = "July";
month[7] = "August";
month[8] = "September";
month[9] = "October";
month[10] = "November";
month[11] = "December";

var APmonth = new Array();
APmonth[0] = "Jan.";
APmonth[1] = "Feb.";
APmonth[2] = "March";
APmonth[3] = "April";
APmonth[4] = "May";
APmonth[5] = "June";
APmonth[6] = "July";
APmonth[7] = "Aug.";
APmonth[8] = "Sept.";
APmonth[9] = "Oct.";
APmonth[10] = "Nov.";
APmonth[11] = "Dec.";

var hours = new Array();
hours[0] = "12";
hours[1] = "1";
hours[2] = "2";
hours[3] = "3";
hours[4] = "4";
hours[5] = "5";
hours[6] = "6";
hours[7] = "7";
hours[8] = "8";
hours[9] = "9";
hours[10] = "10";
hours[11] = "11";
hours[12] = "12";
hours[13] = "1";
hours[14] = "2";
hours[15] = "3";
hours[16] = "4";
hours[17] = "5";
hours[18] = "6";
hours[19] = "7";
hours[20] = "8";
hours[21] = "9";
hours[22] = "10";
hours[23] = "11";

var APhours = new Array();
APhours[0] = "Midnight";
APhours[1] = "1 a.m.";
APhours[2] = "2 a.m.";
APhours[3] = "3 a.m.";
APhours[4] = "4 a.m.";
APhours[5] = "5 a.m.";
APhours[6] = "6 a.m.";
APhours[7] = "7 a.m.";
APhours[8] = "8 a.m.";
APhours[9] = "9 a.m.";
APhours[10] = "10 a.m.";
APhours[11] = "11 a.m.";
APhours[12] = "Noon";
APhours[13] = "1 p.m.";
APhours[14] = "2 p.m.";
APhours[15] = "3 p.m.";
APhours[16] = "4 p.m.";
APhours[17] = "5 p.m.";
APhours[18] = "6 p.m.";
APhours[19] = "7 p.m.";
APhours[20] = "8 p.m.";
APhours[21] = "9 p.m.";
APhours[22] = "10 p.m.";
APhours[23] = "11 p.m.";





















