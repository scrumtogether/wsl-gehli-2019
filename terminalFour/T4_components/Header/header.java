try {
  	importPackage(com.terminalfour.list);
  	importPackage(com.terminalfour.template);
	importClass(org.apache.commons.io.IOUtils);
	importClass(com.terminalfour.media.MediaManager);
  	importClass(com.terminalfour.publish.PathBuilder);
	importClass(com.terminalfour.publish.utils.TreeTraversalUtils);
	importClass(com.terminalfour.content.Content);
	importClass(com.terminalfour.content.ContentHelper);
	importClass(com.terminalfour.sitemanager.cache.CachedContent);
  	importClass(com.terminalfour.sitemanager.cache.CachedSection);
  	importClass(com.terminalfour.list.PredefinedList);
    importClass(com.terminalfour.list.PredefinedListMetaData);
  	importClass(com.terminalfour.list.PredefinedListManagerImpl);
  	var T4_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 168778, language).getMedia()))),
  		VCU_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 189394, language).getMedia()))),
  		UR_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 265744, language).getMedia()))),   
  		pubCache = publishCache.getChannel(),
  		microsite = T4Utils.publishCache.microsite.parentChannel,
        rootPath = T4Utils.publishCache.microsite.baseHref;
    
  	// Get variables and pieces
    var subnav = T4Utils.getSectionInfo.getChildren(section, false);
    var rootLink = T4Utils.brokerUtils.processT4Tag('<t4 type="navigation" name="Plugin - Homepage Path" id="181" />');
    var parentName = T4Utils.brokerUtils.processT4Tag('<t4 type="content" name="Parent or department" output="normal" />');
    var title = T4Utils.brokerUtils.processT4Tag('<t4 type="content" name="Title or school" output="normal" />');
    var mediaManager = MediaManager.getManager();
    
    //var mediaID = content.get("Background image").getID();
    //var mediapath = T4Utils.brokerUtils.processT4Tag('<t4 type="media" id="' + mediaID + '" formatter="path/*"/>');
    var oCH = new ContentHierarchy();
    var oCM = com.terminalfour.spring.ApplicationContextProvider.getBean(com.terminalfour.content.IContentManager);
    var rootSectionID = publishCache.getMicroSiteFromChild(section).getRootSectionID();
    var rootSection = TreeTraversalUtils.findSection(publishCache.getChannel(), section, rootSectionID, language);
    var c = rootSection.getChildren(publishCache.channel, false);
  	var links = '';
  if(content.get('Utility navigation') != ''){
    links= '<ul class="page-header__utility-links">'
  		var menuLink = String(content.get('Utility navigation')).replace('<t4 type="sslink" sslink_id="','').replace('"/>','');
    	link = com.terminalfour.navigation.ServerSideLinkManager.getManager().getLink(dbStatement.getConnection(), Number(menuLink), section.getID(), content.getID(), language),
        sectionID = link.getToSectionID(),
        utilitySection = TreeTraversalUtils.findSection(publishCache.getChannel(), section, sectionID, language);
      	utilityChilds = utilitySection.getChildren(publishCache.channel, false);
    
      for(i=0;i<utilityChilds.length;i++){
        	link = PathBuilder.getLink(dbStatement, utilityChilds[i], publishCache, language, isPreview),
            findLink = /(<a href="([^].*?)")/i,
            URL = findLink.exec(String(link));
        	links = links + '<li>'+link+'</li>';

      }
    
    links = links + '</ul>';
  } else {menuLink = '';}
  
  // Determine the site titles
  if(content.get("Parent or department") == ''){
  	var pageTitle = [
      '<h1 class="page-header__site-title">',
      	content.get("Title or school"),
      '</h1>'].join('\n');
    } else {
    	var pageTitle = [
          '<h1 class="page-header__parent-title">',
			content.get("Parent or department"),
          '</h1>',
          '<h2 class="page-header__site-title">',
          	content.get("Title or school"),
          '</h2>'].join('\n');
    }
    
 
  	// Start Header HTML
  
  
  	html = [
		'<div class="hgroup">',
      		'<div class="l-wrapper">',
      			'<div class="l-gutter">',
      				'<button onclick="document.body.classList.toggle(\'show-utility\')" id="nav-utility__toggle" aria-label="Open or close the utility menu with this button" class="nav-utility__toggle">',
      					'<span class="nav-utility__toggle-icon"><span></span><span></span><span></span><span></span></span>',
      				'</button>',      
      				'<!-- Header > Site Name -->',
      				'<div class="page-header__site-name">',
      					'<a href="' + rootLink + '" class="page-header__parent-name page-header__site-link">',
      					pageTitle,
                        '</a>',
      				'</div>',
      				'<!--/.page-header__site-name-->',
      				'<!--.page-header__utility-->',
      					'<div class="page-header__utility">',
      						links,
      				
      						'<!-- Header > Search -->',
                          	'<form action="https://search.vcu.edu/search" method="get" class="page-header__search">',
      						'<input type="hidden" name="proxystylesheet" value="default_frontend">',
      						'<input type="hidden" name="sitesearch" value="' + rootPath + '">',
      						'<div class="page-header__search-group">',
      							'<label class="page-header__search-label" for="search">Search</label>',
      							'<input class="page-header__search-field" type="text" name="q" placeholder="Search" id="search" />',
      							'<span class="page-header__search-icon"></span>',
      							'<input type="submit" value="Go" />',
      						'</div>',
      					'</form>', // The utility menu below goes here   
      				'</div><!--/.page-header__utility-->',
      			'</div><!--/.page-header__gutter-->',
      		'</div><!--/.page-header__wrapper-->',
    	'</div>'].join('\n');
    
  	document.write(html);
  
} catch (e) {
    if (e instanceof SyntaxError) {
        document.write("<strong>Syntax Error:</strong> " + e.message);
    } else {
        document.write("<strong>T4 Error:</strong> " + e.message);
    }
}
