try {
  importPackage(com.terminalfour.list);
  importPackage(com.terminalfour.template);
  importClass(org.apache.commons.io.IOUtils);
  importClass(com.terminalfour.media.MediaManager);
  importClass(com.terminalfour.publish.PathBuilder);
  importClass(com.terminalfour.publish.utils.TreeTraversalUtils);
  importClass(com.terminalfour.content.Content);
  importClass(com.terminalfour.content.ContentHelper);
  importClass(com.terminalfour.sitemanager.cache.CachedContent);
  importClass(com.terminalfour.sitemanager.cache.CachedSection);
  importClass(com.terminalfour.list.PredefinedList);
  importClass(com.terminalfour.list.PredefinedListMetaData);
  importClass(com.terminalfour.list.PredefinedListManagerImpl);
  //Import utility scripts
  var T4_UTILS = eval(String(IOUtils.toString(MediaManager.manager.get(dbStatement, 168778, language).getMedia())));
  var VCU_UTILS = eval(String(IOUtils.toString(MediaManager.manager.get(dbStatement, 189394, language).getMedia())));
  var UR_UTILS = eval(String(IOUtils.toString(MediaManager.manager.get(dbStatement, 265744, language).getMedia())));
  var WSL_UTILS = eval(String(IOUtils.toString(MediaManager.manager.get(dbStatement, 273416, language).getMedia())));


  var pubCache = publishCache.getChannel();
  // Setup objects and variables
  var oCH = new ContentHierarchy(),
      oCM = com.terminalfour.spring.ApplicationContextProvider.getBean(com.terminalfour.content.IContentManager),
      menuLink = String(content.get('Menu root')).replace('<t4 type="sslink" sslink_id="', '').replace('"/>', ''),
      breakpoint = content.get('Breakpoint'),
      rootSection = TreeTraversalUtils.findSection(publishCache.getChannel(), section, Number(menuLink), language),
      link = com.terminalfour.navigation.ServerSideLinkManager.getManager().getLink(dbStatement.getConnection(), Number(menuLink), section.getID(), content.getID(), language),
      sectionID = link.getToSectionID(),
      rootSection = TreeTraversalUtils.findSection(publishCache.getChannel(), section, sectionID, language),
      childs = sortMenu(rootSection.getChildren(publishCache.channel, true));

  // Publish JSON file    
  T4Utils.brokerUtils.processT4Tag('<t4 type="navigation" name="SYSTEM - JSON DATA" id="4507" />');

  // Determine if this content is in the 
  if(section.getName('en') == 'Site-Header'){
    document.write('<nav id="nav-main" class="nav-main">');
    document.write('<div class="l-wrapper">');
      document.write('<div class="l-gutter">');
        document.write('<button onclick="document.body.classList.toggle(\'show-menu\');document.body.classList.remove(\'show-utility\');document.body.classList.remove(\'show-search\')" id="nav-main__toggle" aria-label="Open or close the menu with this button" class="nav-main__toggle"><span class="nav-main__toggle-icon"><span></span><span></span><span></span><span></span></span></button>');
        
  }else {
    document.write('<nav id="nav-sub" class="nav-sub">');
        document.write('<div class="l-wrapper">');
      document.write('<div class="l-gutter">');
  }
  
  
  
  
    document.write('<ul class="nav-main__tree-level1" role="menubar">');
        for (i = 0; i < childs.length; i++){
          var level = 0,
              c = sortMenu(childs[i].getChildren(publishCache.channel, true)),
              link = PathBuilder.getLink(dbStatement, childs[i], publishCache, language, isPreview),
              findLink = /(<a href="([^].*?)")/i,
              URL = findLink.exec(String(link));     
          // Get all the children and create a string of all the ID's so current page can be determined in the page layout
          childrenIDs = '';
          for (ch = 0; ch < c.length; ch++) {
            childrenIDs = childrenIDs + '{{' + c[ch].getID() + '}}';
            // Get children and add all section ID's for determining .is-current
            thirdLevelChildren = sortMenu(c[ch].getChildren(publishCache.channel, true));
            for (ch3 = 0; ch3 < thirdLevelChildren.length; ch3++) {
              childrenIDs = childrenIDs + '{{' + thirdLevelChildren[ch3].getID() + '}}';
            }
          }
          // Get the ID of the section for this menu item
          current = '{{' + childs[i].getID() + '}}';    
          // Check to see if the section is a section link or notPublishable before rendering the third level
          if (childs[i].isLinkSection() == true || childs[i].isVisibleInNavigation() == true) {
          if (c.length > 0) {
            document.write('<li class="nav-main__tree-item has-dropdown is-collapsed' + current + childrenIDs + '">');
              document.write('<a class="nav-main__tree-link" role="button" href="' + URL[2] + '" aria-label="' + childs[i].getName(language) +'">' + childs[i].getName(language) + '</a>');
                document.write('<button class="nav-main__tree-toggle" onclick="this.parentElement.classList.toggle(\'is-expanded\');this.parentElement.classList.toggle(\'is-collapsed\');" role="button" aria-expanded="false" aria-haspopup="true"><span class="sr-only">Click to expand sub navigation</span><span class="nav-main__tree-caret"></span></button>');
                  document.write('<ul class="nav-main__tree-level2">');
                  for (a = 0; a < c.length; a++) {
                      var link = PathBuilder.getLink(dbStatement, c[a], publishCache, language, isPreview),
                          findLink = /(<a href="([^].*?)")/i;
                      URL2 = findLink.exec(String(link));
                      thiscurrent = '{{' + c[a].getID() + '}}';
                      //Is there a third level??
                      third = sortMenu(c[a].getChildren(publishCache.channel, true));      
                      thirdchildrenIDs = '';
                      // Add all the section ID's for this item and all children to determine if .is-current
                      for (ch = 0; ch < third.length; ch++) {
                        thirdchildrenIDs = thirdchildrenIDs + '{{' + third[ch].getID() + '}}';
                        thirdLevelChildren3 = sortMenu(third[ch].getChildren(publishCache.channel, true));
                        for (ch4 = 0; ch4 < thirdLevelChildren3.length; ch4++) {
                          thirdchildrenIDs = thirdchildrenIDs + '{{' + thirdLevelChildren3[ch4].getID() + '}}';
                        }
                      }
                      // Create third level links
                      if (third.length > 0) {
                        document.write('<li class="nav-main__tree-item has-dropdown is-collapsed' + thiscurrent + thirdchildrenIDs + '">');
                          document.write('<a class="nav-main__tree-link" role="button" href="' + URL2[2] + '" aria-label="' + c[a].getName(language) + ' - ' + childs[i].getName(language) + '">' + c[a].getName(language) + '</a>');
                          var thirdLevelLinks = ''
                          for (t = 0; t < third.length; t++) {
                            if (third[t].isLinkSection() == true || third[t].isVisibleInNavigation() == true) {
                              link = PathBuilder.getLink(dbStatement, third[t], publishCache, language, isPreview);
                              findLink = /(<a href="([^].*?)")/i;
                              URL3 = findLink.exec(String(link));
                              thirdcurrent = '{{' + third[t].getID() + '}}';
                              thirdLevelLinks = thirdLevelLinks + '<li class="nav-main__tree-item' + thirdcurrent + '"><a class="nav-main__tree-link" role="button" href="' + URL3[2] + '" aria-label="' + third[t].getName(language) + ' - ' + c[a].getName(language) + '">' + third[t].getName(language) + '</a></li>';
                            }
                          }
                          // If third level links are present
                          if(thirdLevelLinks != ''){
                            document.write('<button class="nav-main__tree-toggle" onclick="this.parentElement.classList.toggle(\'is-expanded\');this.parentElement.classList.toggle(\'is-collapsed\');" role="button" aria-expanded="false" aria-haspopup="true"><span class="sr-only">Click to expand sub navigation</span><span class="nav-main__tree-caret"></span></button>');
                            document.write('<ul class="nav-main__tree-level3">');
                              document.write(thirdLevelLinks);
                            document.write('</ul>');
                            document.write('</li>');
                          }
                      // No third level
                      } else {
                          document.write('<li class="nav-main__tree-item' + thiscurrent + '"><a class="nav-main__tree-link" role="button" href="' + URL2[2] + '" aria-label="' + c[a].getName(language) + ' - ' + childs[i].getName(language) + '">' + c[a].getName(language) + '</a></li>');
                      }
                  }
                document.write('</ul>');
              } else {
                document.write('<li role="menuitem" class="nav-main__tree-item' + current + '">');
                  document.write('<a class="nav-main__tree-link" role="button" href="' + URL[2] + '" aria-label="' + childs[i].getName(language) + '">' + childs[i].getName(language) + '</a>');
                document.write('</li>');
              }
            }
        }
        document.write('</ul>');
      document.write('</div>');
    document.write('</div>');
  document.write('</nav>');

} catch (e) {
    if (e instanceof SyntaxError) {
        document.write('<strong>Syntax Error:</strong> ' + e.message);
    } else {
        document.write('<strong>T4 Error:</strong> ' + e.message);
    }
}