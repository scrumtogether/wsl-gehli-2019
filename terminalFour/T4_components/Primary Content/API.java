try {
	importPackage(com.terminalfour.template);
	importClass(org.apache.commons.io.IOUtils);
	importClass(com.terminalfour.media.MediaManager);
	importClass(com.terminalfour.publish.utils.TreeTraversalUtils);
	importClass(com.terminalfour.content.Content);
	importClass(com.terminalfour.content.ContentHelper);
	importClass(com.terminalfour.sitemanager.cache.CachedContent);
	importPackage(com.terminalfour.list);
	importPackage(com.terminalfour.template);
	importClass(com.terminalfour.publish.PathBuilder);
	importClass(com.terminalfour.list.PredefinedList);
	importClass(com.terminalfour.list.PredefinedListMetaData);
	importClass(com.terminalfour.list.PredefinedListManagerImpl);
	importClass(java.lang.Thread);
	importClass(com.terminalfour.publish.utils.BrokerUtils);
	importClass(com.terminalfour.publish.utils.PublishUtils);
  
  	var WS_TAG_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 273416, language).getMedia()))),
        WS_SHORTCODE_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 273418, language).getMedia())))

	// Import utility scripts
  /*
	var T4_UTILS = eval(String (com.terminalfour.publish.utils.BrokerUtils.processT4Tags (dbStatement, publishCache, section, null, language, isPreview, '<t4 type="media" id="168778" formatter="inline/*" />'))),
		VCU_UTILS = eval(String (com.terminalfour.publish.utils.BrokerUtils.processT4Tags (dbStatement, publishCache, section, null, language, isPreview, '<t4 type="media" id="189394" formatter="inline/*" />'))),
		WS_LAYOUT_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 273282, language).getMedia()))),
		WS_TAG_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 273416, language).getMedia())));

*/
  document.write('{ "primaryContent" : [{"content" : "'+fixedEncodeURIComponent(doShortcodes(content.get("Content")))+'", "class": "' + content.get("Class") + '", "name": "' + content.get("Name") + '" }] }');
               
               
               
} catch (e) {
    if (e instanceof SyntaxError) {
        document.write('<strong>Syntax Error:</strong> ' + e.message);
    } else {
	      document.write('<strong>T4 Error:</strong> ' + e.message +'<br>'+e);
	}
}



