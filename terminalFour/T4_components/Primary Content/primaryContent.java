try {
  	importPackage(com.terminalfour.list);
  	importPackage(com.terminalfour.template);
	importClass(org.apache.commons.io.IOUtils);
	importClass(com.terminalfour.media.MediaManager);
  	importClass(com.terminalfour.publish.PathBuilder);
	importClass(com.terminalfour.publish.utils.TreeTraversalUtils);
	importClass(com.terminalfour.content.Content);
	importClass(com.terminalfour.content.ContentHelper);
	importClass(com.terminalfour.sitemanager.cache.CachedContent);
  	importClass(com.terminalfour.sitemanager.cache.CachedSection);
  	importClass(com.terminalfour.list.PredefinedList);
    importClass(com.terminalfour.list.PredefinedListMetaData);
  	importClass(com.terminalfour.list.PredefinedListManagerImpl);
  	var T4_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 168778, language).getMedia()))),
  		VCU_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 189394, language).getMedia()))),
  		UR_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 265744, language).getMedia()))),   
  		pubCache = publishCache.getChannel(),
  		microsite = T4Utils.publishCache.microsite.parentChannel,
        rootPath = T4Utils.publishCache.microsite.baseHref,
        priContent = doShortcodes(convertTags(content.get('Content'), content.getID(), section.getID()));
  
  //Processd and export JSON API
  T4Utils.brokerUtils.processT4Tag('<t4 type="navigation" name="SYSTEM - JSON DATA" id="4507" />');

	html = [
      '<section aria-label="' + content.get("Name") + '" class="content-primary ' + content.get("Class") + '">',
      	'<div class="l-wrapper">',
      		'<div class="l-gutter">',
      			'<div class="content-primary__content">',
      				priContent,
				'</div><!--/.-->',
      		'</div><!--/.-->',
      	'</div><!--/.-->',
      '</section><!--/.-->'
      ].join('\n');


} catch (e) {
    if (e instanceof SyntaxError) {
        document.write("<strong>Syntax Error:</strong> " + e.message);
    } else {
        document.write("<strong>T4 Error:</strong> " + e.message);
    }
}