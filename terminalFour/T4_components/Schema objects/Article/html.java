try {
  	importPackage(com.terminalfour.list);
  	importPackage(com.terminalfour.template);
	importClass(org.apache.commons.io.IOUtils);
	importClass(com.terminalfour.media.MediaManager);
  	importClass(com.terminalfour.publish.PathBuilder);
	importClass(com.terminalfour.publish.utils.TreeTraversalUtils);
	importClass(com.terminalfour.content.Content);
	importClass(com.terminalfour.content.ContentHelper);
	importClass(com.terminalfour.sitemanager.cache.CachedContent);
  	importClass(com.terminalfour.sitemanager.cache.CachedSection);
  	importClass(com.terminalfour.list.PredefinedList);
    importClass(com.terminalfour.list.PredefinedListMetaData);
  	importClass(com.terminalfour.list.PredefinedListManagerImpl);
  	var T4_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 168778, language).getMedia()))),
  		VCU_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 189394, language).getMedia()))),  
        WSL_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 273416, language).getMedia()))),
  		WSL_Shortcodes = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 273418, language).getMedia()))),
  		WSL_Conversions = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 273417, language).getMedia())));
  
 	//Processd and export JSON API
  	//T4Utils.brokerUtils.processT4Tag('<t4 type="navigation" name="SYSTEM - JSON DATA" id="4507" />');
    
  	// Create fulltext page with URL created from name of content
  	T4Utils.brokerUtils.processT4Tag('<t4 type="content" name="Name" output="fulltext" modifiers="" use-element="true" filename-element="Name"/>');
  		var pubCache = publishCache.getChannel(),
            chID = publishCache.getChannel().getID(),
            microsite = T4Utils.publishCache.microsite.parentChannel,
            rootPath = T4Utils.publishCache.microsite.baseHref,
            description = '', 
            headline = '', 
            datePublished = '', 
            articleBody = '', 
            fulltextLink = getFullTextLink(section, content),
            thumbnail = '',
            groupFirst = '',
            groupLast = '',
            author = '',
            audience = '',
            articleSection = '';
            
    if(content.hasElement('thumbnailURL') && content.get('thumbnailURL') != ''){
      thumbnail = T4Utils.brokerUtils.processT4Tag('<t4 type="content" name="thumbnailURL" output="normal" formatter="image/*" />');
    }
        //priContent = convertTags(doShortcodes(content.get('Content')), content.getID(), section.getID());
  	if(content.get("headline") != ""){
    	headline = '<div class="news-article__headline">' + content.get("headline") + '</div><!--/.-->';
  	}
  	if(content.get("datePublished") != ""){
      var date = new Date(content.get('datePublished').publish()),
        date = APmonth[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear();
    	datePublished = '<div class="news-article__datePublished">' + date + '</div><!--/.-->';
  	}
  	if(content.get("description") != ""){
    	description = '<div class="news-article__description">' + content.get("description") + '</div><!--/.-->';
  	}
  	if(content.get("articleBody") != ""){
    	articleBody = '<div class="news-articleBody">' + convertTags(doShortcodes(content.get("articleBody")), content.getID(), section.getID()) + '</div><!--/.-->';
  	}
   	if(thumbnail != ""){
    	thumbnail = '<div class="news-article__image">' + thumbnail + '</div><!--/.-->';
  	}
  if(content.get("author") != ""){
    	author = '<div class="news-article__author"><span>Author: </span>' + content.get("author") + '</div><!--/.-->';
  	}
   if(content.get("audience") != ""){
    	audience = '<div class="news-article__audience"><span>Audience: </span>' + content.get("audience") + '</div><!--/.-->';
  	}
   if(content.get("articleSection") != ""){
    	articleSection = '<div class="news-article__articleSection"><span>Article section: </span>' + content.get("articleSection") + '</div><!--/.-->';
  	}
    if(T4Utils.ordinalIndicators.groupFirst && T4Utils.ordinalIndicators.groupCount > 1){
  		groupFirst = '<section class="l-articles"><div class="l-wrapper"><div class="l-gutter">';
    }      
    if(T4Utils.ordinalIndicators.groupLast && T4Utils.ordinalIndicators.groupCount > 1){
      groupLast = '</div></div></section>';
    }

	html = [
      groupFirst,
      '<section aria-label="News article - ' + content.get("headline") + '" class="news-article">',
      	'<div class="l-wrapper">',
      		'<div class="l-gutter">',
      				thumbnail,
      				headline,
      				datePublished,
            		author,
      				audience,
      				articleSection,
      				description,
      			'<div class="news-article__fulltext-link">',
      				'<button><a href="' + fulltextLink + '" aria-label="Read more - ' + content.get("headline") + '">Read more</a></button>',
				'</div><!--/.-->',
      		'</div><!--/.-->',
      	'</div><!--/.-->',
      '</section><!--/.-->',
      groupLast
      ].join('\n');


} catch (e) {
    if (e instanceof SyntaxError) {
        document.write("<strong>Syntax Error:</strong> " + e.message);
    } else {
        document.write("<strong>T4 Error:</strong> " + e.message);
    }
}