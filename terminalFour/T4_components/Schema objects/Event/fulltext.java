try {
  	importPackage(com.terminalfour.list);
  	importPackage(com.terminalfour.template);
	importClass(org.apache.commons.io.IOUtils);
	importClass(com.terminalfour.media.MediaManager);
  	importClass(com.terminalfour.publish.PathBuilder);
	importClass(com.terminalfour.publish.utils.TreeTraversalUtils);
	importClass(com.terminalfour.content.Content);
	importClass(com.terminalfour.content.ContentHelper);
	importClass(com.terminalfour.sitemanager.cache.CachedContent);
  	importClass(com.terminalfour.sitemanager.cache.CachedSection);
  	importClass(com.terminalfour.list.PredefinedList);
    importClass(com.terminalfour.list.PredefinedListMetaData);
  	importClass(com.terminalfour.list.PredefinedListManagerImpl);
  	var T4_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 168778, language).getMedia()))),
  		VCU_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 189394, language).getMedia()))),  
        WSL_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 273416, language).getMedia()))),
  		WSL_Shortcodes = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 273418, language).getMedia()))),
  		WSL_Conversions = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 273417, language).getMedia())));
  
 	//Processd and export JSON API
  	//T4Utils.brokerUtils.processT4Tag('<t4 type="navigation" name="SYSTEM - JSON DATA" id="4507" />');
    
  	// Create fulltext page with URL created from name of content
  		var pubCache = publishCache.getChannel(),
            chID = publishCache.getChannel().getID(),
            microsite = T4Utils.publishCache.microsite.parentChannel,
            rootPath = T4Utils.publishCache.microsite.baseHref,
            image = T4Utils.brokerUtils.processT4Tag('<t4 type="content" name="image" output="normal" formatter="image/*" />'),
            name = '',
            doorTime = '', 
            startDate = '', 
            endDate = '', 
            location = '', 
            sponsor = '', 
            audience = '', 
            description = '';
      
          
	if(content.getName() != ""){
    	name = '<div class="event__name">' + content.getName() + '</div><!--/.-->';
  	}
  	if(content.get('doorTime') != ""){
      var date = new Date(content.get('doorTime').publish());
      if(date.getMinutes() < 10){mins = date.getMinutes() + '0'}else {mins = date.getMinutes()}
          time = APhour[date.getHours()] + ':' + mins + ' ' + AmPm[date.getHours()];
      doorTime = '<div class="event__doorTime"><span>Door time:</span> ' + time + '</div><!--/.-->';
  	}
  	if(content.get('startDate') != ""){
        var date = new Date(content.get('startDate').publish()),
        date = APmonth[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear();
      startDate = '<div class="event__startDate"><span>Start date:</span> ' + date + '</div><!--/.-->';
  	}
	if(content.get('endDate') != ""){
            var date = new Date(content.get('endDate').publish()),
        date = APmonth[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear();
      endDate = '<div class="event__endDate"><span>End date:</span> ' + date + '</div><!--/.-->';
  	}
  	if(content.get('location') != ""){
      location = '<div class="event__location"><span>Location:</span> ' + content.get('location') + '</div><!--/.-->';
  	}
  	if(content.get('sponsor') != ""){
      sponsor = '<div class="event__sponsor"><span>Sponsor:</span> ' + content.get('sponsor') + '</div><!--/.-->';
  	}
  	if(content.get('audience') != ""){
      audience = '<div class="event__audience"><span>Audience:</span> ' + content.get('audience') + '</div><!--/.-->';
  	}
  	if(content.get('description') != ""){
    	description = '<div class="event__description">' + content.get('description') + '</div><!--/.-->';
  	}
  	if(content.get('image') != ""){
    	image = '<div class="event__image">' + image + '</div><!--/.-->';
  	}
  
	html = [
      '<section aria-label="Event - ' + content.getName() + '" class="event l-fulltext">',
      	'<div class="l-wrapper">',
      		'<div class="l-gutter">',
      				name,
      				image,
      				description,
                    doorTime,
                    startDate,
                    endDate,
                    location,
                    sponsor,
                    audience,
      '<p><a href="#" class="object__back-button" onclick="window.history.back()">&larr; back</a></p>',
      		'</div><!--/.-->',
      	'</div><!--/.-->',
      '</section><!--/.-->'
      ].join('\n');


} catch (e) {
    if (e instanceof SyntaxError) {
        document.write("<strong>Syntax Error:</strong> " + e.message);
    } else {
        document.write("<strong>T4 Error:</strong> " + e.message);
    }
}