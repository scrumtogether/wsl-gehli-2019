try {
  	importPackage(com.terminalfour.list);
  	importPackage(com.terminalfour.template);
	importClass(org.apache.commons.io.IOUtils);
	importClass(com.terminalfour.media.MediaManager);
  	importClass(com.terminalfour.publish.PathBuilder);
	importClass(com.terminalfour.publish.utils.TreeTraversalUtils);
	importClass(com.terminalfour.content.Content);
	importClass(com.terminalfour.content.ContentHelper);
	importClass(com.terminalfour.sitemanager.cache.CachedContent);
  	importClass(com.terminalfour.sitemanager.cache.CachedSection);
  	importClass(com.terminalfour.list.PredefinedList);
    importClass(com.terminalfour.list.PredefinedListMetaData);
  	importClass(com.terminalfour.list.PredefinedListManagerImpl);
  	var T4_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 168778, language).getMedia()))),
  		VCU_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 189394, language).getMedia()))),  
        WSL_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 273416, language).getMedia()))),
  		WSL_Shortcodes = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 273418, language).getMedia()))),
  		WSL_Conversions = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 273417, language).getMedia())));
  
 	//Processd and export JSON API
  	//T4Utils.brokerUtils.processT4Tag('<t4 type="navigation" name="SYSTEM - JSON DATA" id="4507" />');
    
  	// Create fulltext page with URL created from name of content
  		var pubCache = publishCache.getChannel(),
            chID = publishCache.getChannel().getID(),
            microsite = T4Utils.publishCache.microsite.parentChannel,
            rootPath = T4Utils.publishCache.microsite.baseHref,
            image = T4Utils.brokerUtils.processT4Tag('<t4 type="content" name="image" output="normal" formatter="image/*" />'),
            name = '',
            doorTime = '', 
            startDate = '', 
            endDate = '', 
            location = '', 
            sponsor = '', 
            audience = '', 
            description = '',
            disambiguatingDescription,
            fulltextLink = getFullTextLink(section, content);
      
          
	if(content.getName() != ""){
    	name = '<div class="event__name">' + content.getName() + '</div><!--/.-->';
  	}
    if(content.hasElement('doorTime')){
      if(content.get('doorTime') != ""){
          doorTime = '<div class="event__doorTime">' + content.get('doorTime').publish() + '</div><!--/.-->';
      }
    }
  	if(content.get('startDate') != ""){
      var date = new Date(content.get('startDate').publish()),
        date = APmonth[date.getMonth()] + ' ' + date.getDate() + ', ' + date.getFullYear();
    	startDate = '<div class="event__startDate">' + date + '</div><!--/.-->';
  	}
	if(content.get('endDate') != ""){
    	endDate = '<div class="event__endDate">' + content.get('endDate').publish() + '</div><!--/.-->';
  	}
  	if(content.get('location') != ""){
    	location = '<div class="event__location">' + content.get('location') + '</div><!--/.-->';
  	}
  	if(content.get('sponsor') != ""){
    	sponsor = '<div class="event__sponsor">' + content.get('sponsor') + '</div><!--/.-->';
  	}
  	if(content.get('audience') != ""){
    	audience = '<div class="event__audience">' + content.get('audience') + '</div><!--/.-->';
  	}
  	if(content.get('description') != ""){
    	description = '<div class="event__description">' + content.get('description') + '</div><!--/.-->';
  	}
  	if(content.get('image') != ""){
    	image = '<div class="event__image">' + image + '</div><!--/.-->';
  	}
    if(content.get('disambiguatingDescription') != ""){
    	disambiguatingDescription = '<div class="event__disambiguatingDescription">' + content.get("disambiguatingDescription") + '</div><!--/.-->';
  	}
  
	html = [
      '<div aria-label="Event - ' + content.getName() + '" class="swiper-slide">',
      	'<div class="l-wrapper">',
      		'<div class="l-gutter">',
                image,
      			startDate,
                name,
                location,
                disambiguatingDescription,
            	'<div class="event__fulltext-link">',
      				'<button><a href="'+fulltextLink+'" aria-label="Read more about -' + content.getName() + '" >Read more</a></button>',
				'</div><!--/.-->',
      		'</div><!--/.-->',
      	'</div><!--/.-->',
      '</div><!--/.-->'
      ].join('\n');


} catch (e) {
    if (e instanceof SyntaxError) {
        document.write("<strong>Syntax Error:</strong> " + e.message);
    } else {
        document.write("<strong>T4 Error:</strong> " + e.message);
    }
}