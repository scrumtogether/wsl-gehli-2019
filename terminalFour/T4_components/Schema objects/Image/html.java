try {
  	importPackage(com.terminalfour.list);
  	importPackage(com.terminalfour.template);
	importClass(org.apache.commons.io.IOUtils);
	importClass(com.terminalfour.media.MediaManager);
  	importClass(com.terminalfour.publish.PathBuilder);
	importClass(com.terminalfour.publish.utils.TreeTraversalUtils);
	importClass(com.terminalfour.content.Content);
	importClass(com.terminalfour.content.ContentHelper);
	importClass(com.terminalfour.sitemanager.cache.CachedContent);
  	importClass(com.terminalfour.sitemanager.cache.CachedSection);
  	importClass(com.terminalfour.list.PredefinedList);
    importClass(com.terminalfour.list.PredefinedListMetaData);
  	importClass(com.terminalfour.list.PredefinedListManagerImpl);
  	var T4_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 168778, language).getMedia()))),
  		VCU_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 189394, language).getMedia()))),  
        WSL_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 273416, language).getMedia()))),
  		WSL_Shortcodes = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 273418, language).getMedia()))),
  		WSL_Conversions = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 273417, language).getMedia())));
  
 	//Processd and export JSON API
  	//T4Utils.brokerUtils.processT4Tag('<t4 type="navigation" name="SYSTEM - JSON DATA" id="4507" />');
    
  	// Create fulltext page with URL created from name of content
  	T4Utils.brokerUtils.processT4Tag('<t4 type="content" name="Name" output="fulltext" modifiers="" use-element="true" filename-element="Name"/>');
  		var pubCache = publishCache.getChannel(),
            chID = publishCache.getChannel().getID(),
            microsite = T4Utils.publishCache.microsite.parentChannel,
            rootPath = T4Utils.publishCache.microsite.baseHref,
            fulltextLink = getFullTextLink(section, content),
            image = '',
            thumbnail = '',
            caption = content.get("caption"),
            text = content.get("text"),
            typeIndex = T4Utils.ordinalIndicators.pageIndex,
            representativeOfPage = '',
            groupFirst = '',
            groupLast = '';
  
  	if(content.get("representativeOfPage").publish() == 'true'){representativeOfPage = ' image-object__representativeOfPage'}
            
    if(content.hasElement('image') && content.get('image') != ''){
      image = contentImagePath(content.get('image'))
    }
  	if(content.hasElement('thumbnail') && content.get('thumbnail') != ''){
      thumbnail = contentImagePath(content.get('thumbnail'))
    }
	if(image != ""){
    	image = '<div class="image-object__image"><img src="' + image + '" alt="'+content.getName()+'" /></div><!--/.-->';
  	}
  	if(thumbnail != ""){
    	thumbnail = '<div class="image-object__thumbnail"><img src="' + thumbnail + '" alt="'+content.getName()+'" /></div><!--/.-->';
  	}
  	if(caption != ""){
    	caption = '<div class="image-object__caption">' + caption + '</div><!--/.-->';
  	}
  	if(text != ""){
    	text = '<div class="image-object__text">' + text + '</div><!--/.-->';
  	}
  
          
    if(T4Utils.ordinalIndicators.groupFirst && T4Utils.ordinalIndicators.groupCount > 1){
      groupFirst = '<section class="l-image-objects"><div class="l-wrapper"><div class="l-gutter">';
    }      
    if(T4Utils.ordinalIndicators.groupLast && T4Utils.ordinalIndicators.groupCount > 1){
      groupLast = '</div></div></section>';
    }

	html = [
      groupFirst,
      //'<script>console.log("' + content.get("representativeOfPage").publish() + '")</script>',
      '<section aria-label="Image - ' + content.getName() + '" class="image-object cid__' + content.getID() + ' page-index__' + typeIndex + representativeOfPage + '">',
      	'<div class="l-wrapper">',
      		'<div class="l-gutter">',
      				image,
      				thumbnail,
      				caption,
      				text,
      		'</div><!--/.-->',
      	'</div><!--/.-->',
      '</section><!--/.-->',
      groupLast
      ].join('\n');


} catch (e) {
    if (e instanceof SyntaxError) {
        document.write("<strong>Syntax Error:</strong> " + e.message);
    } else {
        document.write("<strong>T4 Error:</strong> " + e.message);
    }
}