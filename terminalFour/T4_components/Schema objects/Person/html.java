try {
  	importPackage(com.terminalfour.list);
  	importPackage(com.terminalfour.template);
	importClass(org.apache.commons.io.IOUtils);
	importClass(com.terminalfour.media.MediaManager);
  	importClass(com.terminalfour.publish.PathBuilder);
	importClass(com.terminalfour.publish.utils.TreeTraversalUtils);
	importClass(com.terminalfour.content.Content);
	importClass(com.terminalfour.content.ContentHelper);
	importClass(com.terminalfour.sitemanager.cache.CachedContent);
  	importClass(com.terminalfour.sitemanager.cache.CachedSection);
  	importClass(com.terminalfour.list.PredefinedList);
    importClass(com.terminalfour.list.PredefinedListMetaData);
  	importClass(com.terminalfour.list.PredefinedListManagerImpl);
  	var T4_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 168778, language).getMedia()))),
  		VCU_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 189394, language).getMedia()))),  
        WSL_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 273416, language).getMedia()))),
  		WSL_Shortcodes = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 273418, language).getMedia()))),
  		WSL_Conversions = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 273417, language).getMedia())));

  	// Create fulltext page with URL created from name of content
  	T4Utils.brokerUtils.processT4Tag('<t4 type="content" name="Name" output="fulltext" modifiers="" use-element="true" filename-element="Name"/>');
    
  		var pubCache = publishCache.getChannel(),
            chID = publishCache.getChannel().getID(),
            microsite = T4Utils.publishCache.microsite.parentChannel,
            rootPath = T4Utils.publishCache.microsite.baseHref,
            image = T4Utils.brokerUtils.processT4Tag('<t4 type="content" name="image" output="normal" formatter="image/*" />'),
            name = '',
            fulltextLink = getFullTextLink(section, content),
            name = '',
            email = '',
            affiliation = '',
            jobTitle = '',
            workLocation = '',
            worksFor = '',
            alternateName = '',
            telephone = '',
            disambiguatingDescription = '',
            description = '',
            groupFirst = '',
            groupLast = '';
  
if(content.getName() != ''){name = '<h2 class="person__name">' + content.getName() + '</h2>'};
  
if(content.get("email") != ''){email = '<div class="person__email"><span>Email: </span><a href="mailto:' + content.get("email") + '" alt="Email "' + content.getName() + '">' + content.get("email") + '</a></div>'};
  
if(content.get("affiliation") != ''){affiliation = '<div class="person__affiliation"><span>Affiliation: </span>' + content.get("affiliation") + '</div>'};
  
if(content.get("jobTitle") != ''){jobTitle = '<div class="person__jobTitle"><span>Job title: </span>' + content.get("jobTitle") + '</div>'};
  
if(content.get("workLocation") != ''){workLocation = '<div class="person__workLocation"><span>Location: </span>' + content.get("workLocation") + '</div>'};
  
if(content.get("worksFor") != ''){worksFor = '<div class="person__worksFor"><span>Works for: </span>' + content.get("worksFor") + '</div>'};
  
if(content.get("alternateName") != ''){alternateName = '<div class="person__alternateName"><span>Alternamte name: </span>' + content.get("alternateName") + '</div>'};
  
if(content.get("telephone") != ''){telephone = '<div class="person__telephone"><span>Telephone: </span>' + content.get("telephone") + '</div>'};
  
if(content.get("disambiguatingDescription") != ''){disambiguatingDescription = '<div class="person__disambiguatingDescription"><p>' + content.get("disambiguatingDescription") + '</p></div>',
  '<div class="person__description">' + content.get("description") + '</div>'};
      
  if(T4Utils.ordinalIndicators.groupFirst && T4Utils.ordinalIndicators.groupCount > 1){
  	groupFirst = '<section class="l-persons"><div class="l-wrapper"><div class="l-gutter">';
  }      
  if(T4Utils.ordinalIndicators.groupLast && T4Utils.ordinalIndicators.groupCount > 1){
  	groupLast = '</div></div></section>';
  }
	html = [
      groupFirst,
      '<section aria-label="Event - ' + content.getName() + '" class="person">',
      	'<div class="l-wrapper">',
      		'<div class="l-gutter">',
      			'<div class="person-details">',
                  image,
                  name,
                  alternateName,
                  jobTitle,
                  affiliation,
                  email,
                  telephone,
                  workLocation,
                  worksFor,
                  disambiguatingDescription,
                  '<div class="event__fulltext-link">',
                      '<button><a href="'+fulltextLink+'" aria-label="Read more about -' + content.getName() + '" >Read more</a></button>',
                  '</div><!--/.-->',
      			'</div><!--/.-->',
      		'</div><!--/.-->',
      	'</div><!--/.-->',
      '</section><!--/.-->',
      groupLast
      ].join('\n');


} catch (e) {
    if (e instanceof SyntaxError) {
        document.write("<strong>Syntax Error:</strong> " + e.message);
    } else {
        document.write("<strong>T4 Error:</strong> " + e.message);
    }
}