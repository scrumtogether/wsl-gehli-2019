try {
  	importPackage(com.terminalfour.list);
  	importPackage(com.terminalfour.template);
	importClass(org.apache.commons.io.IOUtils);
	importClass(com.terminalfour.media.MediaManager);
  	importClass(com.terminalfour.publish.PathBuilder);
	importClass(com.terminalfour.publish.utils.TreeTraversalUtils);
	importClass(com.terminalfour.content.Content);
	importClass(com.terminalfour.content.ContentHelper);
	importClass(com.terminalfour.sitemanager.cache.CachedContent);
  	importClass(com.terminalfour.sitemanager.cache.CachedSection);
  	importClass(com.terminalfour.list.PredefinedList);
    importClass(com.terminalfour.list.PredefinedListMetaData);
  	importClass(com.terminalfour.list.PredefinedListManagerImpl);
  	var T4_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 168778, language).getMedia()))),
  		VCU_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 189394, language).getMedia()))),
  		UR_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 265744, language).getMedia()))),   
  		pubCache = publishCache.getChannel(),
  		microsite = T4Utils.publishCache.microsite.parentChannel,
        rootPath = T4Utils.publishCache.microsite.baseHref,
        priContent = doShortcodes(convertTags(content.get('Content'), content.getID(), section.getID()));
 
		html = [
          
          '<meta property="title" content="' + content.get('Titile') + '">',
          '<meta property="description" content="' + content.get('Description') + '">',
          '<meta property="og:title" content="' + content.get('Titile') + '">',
          '<meta property="og:description" content="' + content.get('Description') + '">',
          '<meta property="og:image" content="">',
          '<meta name="twitter:card" content="summary_large_image">',
          '<meta name="twitter:site" content="@vcu">',
          '<meta name="twitter:creator" content="@vcu">',
          '<meta name="twitter:image:alt" content="Virginia Commonwealth University">',
          '<meta name="twitter:title" content="' + content.get('Titile') + '">',
          '<meta name="twitter:image" content="">',
          '<meta name="twitter:description" content="' + content.get('Description') + '">'
          
          ].join('\n');
  
  
  
} catch (e) {
    if (e instanceof SyntaxError) {
        document.write("<strong>Syntax Error:</strong> " + e.message);
    } else {
        document.write("<strong>T4 Error:</strong> " + e.message);
    }
}