try {
	importPackage(com.terminalfour.template);
	importClass(org.apache.commons.io.IOUtils);
	importClass(com.terminalfour.media.MediaManager);
	importClass(com.terminalfour.publish.utils.TreeTraversalUtils);
	importClass(com.terminalfour.content.Content);
	importClass(com.terminalfour.content.ContentHelper);
	importClass(com.terminalfour.sitemanager.cache.CachedContent);
	importPackage(com.terminalfour.list);
	importPackage(com.terminalfour.template);
	importClass(com.terminalfour.publish.PathBuilder);
	importClass(com.terminalfour.list.PredefinedList);
	importClass(com.terminalfour.list.PredefinedListMetaData);
	importClass(com.terminalfour.list.PredefinedListManagerImpl);
	importClass(java.lang.Thread);
	importClass(com.terminalfour.publish.utils.BrokerUtils);
	importClass(com.terminalfour.publish.utils.PublishUtils);

	// Import utility scripts
	var T4_UTILS = eval(String (com.terminalfour.publish.utils.BrokerUtils.processT4Tags (dbStatement, publishCache, section, null, language, isPreview, '<t4 type="media" id="168778" formatter="inline/*" />'))),
		VCU_UTILS = eval(String (com.terminalfour.publish.utils.BrokerUtils.processT4Tags (dbStatement, publishCache, section, null, language, isPreview, '<t4 type="media" id="189394" formatter="inline/*" />'))),
		WS_LAYOUT_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 273282, language).getMedia()))),
		WS_TAG_UTILS = eval(String (IOUtils.toString(MediaManager.manager.get(dbStatement, 273416, language).getMedia())));

	// Get/Set variables and pieces
	var header = T4Utils.brokerUtils.processT4Tag('<t4 type="navigation" name="Plugin - Site Header" id="3384" />'),
		head = T4Utils.brokerUtils.processT4Tag('<t4 type="navigation" name="Plugin - Site Head" id="3664" />'),
		systemCSS = T4Utils.brokerUtils.processT4Tag('<t4 type="media" id="274056" formatter="path/*" />'),
		systemCSSVersion = MediaManager.manager.get(dbStatement, 274056, language).getContent().getVersion(),
		aside = T4Utils.brokerUtils.processT4Tag('<t4 type="navigation" name="SYSTEM - ASIDE Content" id="4520" />');
		T4Utils.brokerUtils.processT4Tag('<t4 type="navigation" name="SYSTEM - JSON DATA" id="4507" />');

	// Build page
	document.write('<!doctype html>');
	document.write('<html class="no-js ws" lang="en">'); 
  document.write('<head>');
	
	if(rootSectionID === sectionID){
		document.write('<title>' + rootSection.getName('en') + ' &mdash; Virginia Commonwealth University</title>');
	} else {
		document.write('<title>' + sectionName + ' &mdash; ' + rootSection.getName('en') + '</title>');
	}
	
	// Get the ICO and Apple Touch icons
	document.write(getIcons());
  document.write('<meta charset="utf-8"/>');  
  document.write('<meta name="viewport" content="width=device-width, initial-scale=1"/>');

	// Process the META content
	document.write(getMETA());

	// Load the compiled CSS file created through a NODE.js Sass workflow
if(isPreview){
	document.write('<link rel="stylesheet" type="text/css" href="' + systemCSS + '" />');
} else {
	document.write('<link rel="stylesheet" type="text/css" href="' + systemCSS + '?ver=' + systemCSSVersion + '" />');
}
	
	// Get the content from the 'Site-Head' hidden section
	document.write(head);

	document.write('</head>');  
  document.write('<body id="ws-' +  section.getID() + '" >');
  document.write('<!--[if lt IE 10]><p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p><![endif]-->');
	
	document.write('<!-- Skip Menu: A list of skip links to jump directly to the primary navigation or content of an interface.-->');
	skipMenu = [
		'<ul class="skip-menu skip-menu__off-screen-ul" role="complementary" aria-label="Skip links menu">',
			'<li class="skip-menu__skip-item">',
				'<a href="#page-main" tabindex="1" class="skip-menu__skip-link">',
					'Skip to content',
				'</a>',
			'</li>',
			'<li class="skip-link__skip-item">',
				'<a href="#page-header__menu" tabindex="2" class="skip-menu__skip-link">',
					'Skip to navigation',
				'</a>',
			'</li>',
			'<li class="skip-menu__skip-item">',
				'<a href="#page-footer" tabindex="3" class="skip-menu__skip-link">',
					'Skip to footer',
				'</a>',
			'</li>',
		'</ul>'
	].join('\n');

	document.write(skipMenu);

  document.write('<!-- Header: Contains the site name, search and optional nav.-->');
  document.write('<header>');
	
		// Get the content from the 'Site-Header' hidden section
  	document.write(header);

  document.write('</header><!--/.page-header-->');


  document.write('<!-- Page Content -->');
	document.write('<main role="main" id="page-main" class="page-main">');
		document.write('<div class="page-main__wrapper">');
			document.write('<div class="page-main__gutter">');

				if(aside != ''){
					document.write(aside);
				}

} catch (e) {
    if (e instanceof SyntaxError) {
        document.write('<strong>Syntax Error:</strong> ' + e.message);
    } else {
	      document.write('<strong>T4 Error:</strong> ' + e.message +'<br>'+e);
	}
}



