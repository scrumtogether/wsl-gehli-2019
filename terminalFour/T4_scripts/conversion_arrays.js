var status = new Array();
status[0] = "Approved";
status[1] = "Pending";

var weekday = new Array();
weekday[0] = "Sunday";
weekday[1] = "Monday";
weekday[2] = "Tuesday";
weekday[3] = "Wednesday";
weekday[4] = "Thursday";
weekday[5] = "Friday";
weekday[6] = "Saturday";

var APweekday = new Array();
APweekday[0] = "Sun.";
APweekday[1] = "Mon.";
APweekday[2] = "Tue.";
APweekday[3] = "Wed.";
APweekday[4] = "Thur.";
APweekday[5] = "Fri.";
APweekday[6] = "Sat.";

var month = new Array();
month[0] = "January";
month[1] = "February";
month[2] = "March";
month[3] = "April";
month[4] = "May";
month[5] = "June";
month[6] = "July";
month[7] = "August";
month[8] = "September";
month[9] = "October";
month[10] = "November";
month[11] = "December";

var APmonth = new Array();
APmonth[0] = "Jan.";
APmonth[1] = "Feb.";
APmonth[2] = "March";
APmonth[3] = "April";
APmonth[4] = "May";
APmonth[5] = "June";
APmonth[6] = "July";
APmonth[7] = "Aug.";
APmonth[8] = "Sept.";
APmonth[9] = "Oct.";
APmonth[10] = "Nov.";
APmonth[11] = "Dec.";

var hours = new Array();
hours[0] = "12";
hours[1] = "1";
hours[2] = "2";
hours[3] = "3";
hours[4] = "4";
hours[5] = "5";
hours[6] = "6";
hours[7] = "7";
hours[8] = "8";
hours[9] = "9";
hours[10] = "10";
hours[11] = "11";
hours[12] = "12";
hours[13] = "1";
hours[14] = "2";
hours[15] = "3";
hours[16] = "4";
hours[17] = "5";
hours[18] = "6";
hours[19] = "7";
hours[20] = "8";
hours[21] = "9";
hours[22] = "10";
hours[23] = "11";

var APhours = new Array();
APhours[0] = "Midnight";
APhours[1] = "1 a.m.";
APhours[2] = "2 a.m.";
APhours[3] = "3 a.m.";
APhours[4] = "4 a.m.";
APhours[5] = "5 a.m.";
APhours[6] = "6 a.m.";
APhours[7] = "7 a.m.";
APhours[8] = "8 a.m.";
APhours[9] = "9 a.m.";
APhours[10] = "10 a.m.";
APhours[11] = "11 a.m.";
APhours[12] = "Noon";
APhours[13] = "1 p.m.";
APhours[14] = "2 p.m.";
APhours[15] = "3 p.m.";
APhours[16] = "4 p.m.";
APhours[17] = "5 p.m.";
APhours[18] = "6 p.m.";
APhours[19] = "7 p.m.";
APhours[20] = "8 p.m.";
APhours[21] = "9 p.m.";
APhours[22] = "10 p.m.";
APhours[23] = "11 p.m.";