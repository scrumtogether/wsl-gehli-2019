  // Determine parent channel with microsite

  var microsite = T4Utils.publishCache.microsite.parentChannel,
        rootPath = T4Utils.publishCache.microsite.baseHref,
    micro = /(id=([^].*?),)/i,
    msid = micro.exec(String(microsite)),
    rootSectionID =  T4Utils.publishCache.microsite.rootSection.id || T4Utils.publishCache.channel.rootSection.id,
    rootSection = TreeTraversalUtils.findSection(publishCache.getChannel(), section, rootSectionID, language),
        siteName = rootSection.getName(language),
        c = rootSection.getChildren(publishCache.channel, false),
        sc = section.getChildren(publishCache.channel, false),
        sectionID = section.getID(),
        rootPathLink = PathBuilder.getLink(dbStatement, rootSection, publishCache, language, isPreview),
        sectionName = section.getName('en'),
        oCH = new ContentHierarchy(),
        oCM = com.terminalfour.spring.ApplicationContextProvider.getBean(com.terminalfour.content.IContentManager),
        childs = rootSection.getChildren(publishCache.channel, true),
        content = oCH.getContent(dbStatement,sectionID,"en"),
        manifest = '';
        //findRootLink = /(<a href="([^].*?)")/i,
        //rootPath = findRootLink.exec(String(rootPathLink));
    rootPath = rootPath.substring(0, rootPath.length - 1);

  // Sort child menu items
    childs.sort(function (a, b) { // Sort the sections into proper sequence
      return a.getPrintSequence() - b.getPrintSequence();
    });
  
  // Get the various icons for the HEAD
  function getIcons(){
    var ico = '<t4 type="media" id="273419" formatter="path/*"/>';
      ico =  T4Utils.brokerUtils.processT4Tag(ico);
    var AT1 = '<t4 type="media" id="273422" formatter="path/*"/>';
      AT1 =  T4Utils.brokerUtils.processT4Tag(AT1);
    var AT2 = '<t4 type="media" id="273421" formatter="path/*"/>';
      AT2 =  T4Utils.brokerUtils.processT4Tag(AT2);
    var AT3 = '<t4 type="media" id="273420" formatter="path/*"/>';
      AT3 =  T4Utils.brokerUtils.processT4Tag(AT3);

    var icons = [
      '<link rel="icon" href="'+ico+'" type="image/x-icon">',
      '<link rel="apple-touch-icon-precomposed" sizes="72x72" href="'+AT1+'">',
      '<link rel="apple-touch-icon-precomposed" sizes="114x114" href="'+AT2+'">',
      '<link rel="apple-touch-icon-precomposed" sizes="144x144" href="'+AT3+'">'
    ].join('\n');

    return icons;
  }

  // Gather all the data we know about this page and create the meta information
  function getMETA(){
  // Check sections META entry feilds and gather the data if entered
    var META_keywords = section.getMetaInfoValue(1, language),
            META_abstract = section.getMetaInfoValue(2, language),
            META_author = section.getMetaInfoValue(3, language),
            META_copyright = section.getMetaInfoValue(4, language),
            META_title = section.getMetaInfoValue(5, language),
            META_description = section.getMetaInfoValue(6, language),
            META_lang = section.getMetaInfoValue(7, language),
            META_modified = section.getMetaInfoValue(8, language),
            META_format = author = section.getMetaInfoValue(9, language),
            META_date = section.getMetaInfoValue(10, language),
            META_contributor = section.getMetaInfoValue(11, language),
          META_last_updated = section.getMetaInfoValue(12, language),
          META_date_updated = section.getMetaInfoValue(13, language),
            META_OG_image = section.getMetaInfoValue(14, language),
            META_OG_title = section.getMetaInfoValue(15, language),
            META_OG_url = section.getMetaInfoValue(16, language),
          META_OG_description = section.getMetaInfoValue(17, language),
            META_twitter_card = section.getMetaInfoValue(21, language),
            META_twitter_creator = section.getMetaInfoValue(22, language),
            META_twitter_site = section.getMetaInfoValue(20, language),
            META = '',
          OGMETA = '',
          TWITTERMETA = '';
        
      if(META_keywords != null) {META = META + '<meta name="keywords" content="' + META_keywords + '" />' }
      if(META_abstract != null){META = META + '<meta name="abstract" content="' + META_abstract + '" />'}
      if(META_author != null){META = META + '<meta name="author" content="' + META_author + '" />'}
      if(META_copyright != null){META = META + '<meta name="copyright" content="' + META_copyright + '" />'}
      if(META_title != null){META = META + '<meta name="title" content="' + META_title + '" />'} else {META = META + '<meta name="title" content="' + section.getName(language) + '" />'}
      if(META_description != null){META = META + '<meta name="description" content="' + META_description + '" />'} else {META = META + '<meta name="description" content="' + section.getName(language) + '" />'}
      if(META_lang != null){META = META + '<meta name="langauge" content="' + META_lang + '" />'}
      if(META_modified != null){META = META + '<meta name="modified" content="' + META_modified + '" />'}
      if(META_format != null){META = META + '<meta name="format" content="' + META_format + '" />'}
      if(META_date != null){META = META + '<meta name="date" content="' + META_date + '" />'}
      if(META_contributor != null){META = META + '<meta name="contributor" content="' + META_contributor + '" />'}
      var link = PathBuilder.getLink(dbStatement, section, publishCache, language, isPreview);
      var findLink = /(<a href="([^].*?)")/i,
            thissectionlink = findLink.exec(String(link));

      var METAImage =  T4Utils.brokerUtils.processT4Tag('<t4 type="media" id="274728" formatter="path/*"/>'),
          imageDesc = MediaManager.manager.get(dbStatement, 274728, language).getDescription();
      if(isPreview != true){
          METAImage = rootPath+METAImage;
      }
  
      // Not a fulltext page, so get the values from the T4 META fields. If not filled in, use the available required fields
      // check for a news or events feed on the page and add the rss link for auto discovery
      // Check for default section META data for overrides to generic info

    var con = oCH.getContent(dbStatement,section.getID(),language);
      var found = false;
      for(var x=0;x < con.length;x++){
      // Loop through all the content on the section and find the social share widget image to use for META
        var piece = oCM.get(con[x],language);
        if(piece.getContentTypeID() === 4182){
          manifest = '<link rel="manifest" href="/manifest.json">';
        }
          
        if(piece.getContentTypeID() === 4008 && found == false){
      // Get the image if there is one. 
            found = true;
            var mediaTag = '<t4 type="media" id="' + piece.get('OG:Image').getID() + '" formatter="path/*"/>';
          var img =  T4Utils.brokerUtils.processT4Tag(mediaTag);
            var mediaManager = MediaManager.getManager();
            var imgObj =  mediaManager.get(dbStatement.getConnection(), piece.get('OG:Image').getID(), language)
            imageDesc = imgObj.getDescription();
          
            //META = META + '<META follow="' + piece.get("No follow").publish() + '" index="' + piece.get("No index").publish() + '" >'; 
            
            if(piece.get('No follow').publish() == 'true' && piece.get('No index').publish() == 'true'){
                META = META + '<META NAME="ROBOTS" CONTENT="NOINDEX, NOFOLLOW">';
            } else if (piece.get('No follow').publish() == 'true' && piece.get('No index').publish() == 'false'){
                META = META + '<META NAME="ROBOTS" CONTENT="NOFOLLOW">';
            } else if (piece.get('No follow').publish() == 'false' && piece.get('No index').publish() == 'true'){
              META = META + '<META NAME="ROBOTS" CONTENT="NOINDEX">';
            }
            
              
            if(isPreview != true){
              METAImage = rootPath+img;

            } else {
              METAImage = img;       
            }
            METAImage = METAImage.replace('http:','https:');
        }
      }
     
        if(isPreview != true){
          pageURL = rootPath+thissectionlink[2]; 
        } else {
            pageURL = thissectionlink[2]; 
        }
        if(META_description == null){
          META_description = section.getName(language);
        }
        if(META_title == null){
          META_title = section.getName(language);
        }
        if(META_OG_title == null){
          META_OG_title = META_title;
        }
        if(META_OG_description == null){
          META_OG_description = META_description;
        }
        if(META_OG_url == null){
          META_OG_url = pageURL;
        }
        if(META_twitter_site == null){
          META_twitter_site = '@vcu';
        }
        if(META_twitter_site == null){
          META_twitter_site = '@vcu';
        }
        if(META_twitter_card == null){
          META_twitter_card = 'summary_large_image';
        }
        if(META_twitter_creator == null){
          META_twitter_creator= '@vcu';
        }
      
        // If this is fulltext layout then go and get the specific information from the content else fill in with available section information
        if(com.terminalfour.publish.utils.BrokerUtils.isFullTextPage(publishCache)) {
            var fullTextContentId = publishCache.getGenericProp('full-text-' + Thread.currentThread().getId()).getContentID();
            var fullTextContent = oCM.get(String(fullTextContentId),language);
            var contentTypeID = fullTextContent.getContentTypeID();

            // Logic for adding data from people, news, or events fulltext views later

        }
            
        var meta = [
          '<meta name="theme-color" content="#000000">',
          META,
          '<meta property="og:type" content="website" />',
          '<meta property="og:locale" content="en_US" />',
          '<meta property="og:title" content="' + META_OG_title + '" />',
          '<meta property="og:description" content="' + META_OG_description + '" />',
          '<meta property="og:image" content="' + METAImage + '" />',
          '<meta property="og:url" content="' + META_OG_url + '" />',
          '<meta property="og:site_name" content="' + siteName + '" />',
          '<meta name="twitter:card" content="' + META_twitter_card + '" />',
          '<meta name="twitter:site" content="' + META_twitter_site + '" />',
          '<meta name="twitter:creator" content="' + META_twitter_creator + '">',
          '<meta name="twitter:image:alt" content="'+imageDesc+'">',
          '<meta name="twitter:title" content="' + META_OG_title + '">',
          '<meta name="twitter:image" content="' + METAImage + '">',
          '<meta name="twitter:description" content="' + META_OG_description + '">',
          '<meta name="geo.region" content="US-VA" />',
          '<meta name="geo.placename" content="Richmond" />'  
        ].join('\n');
      

      // JSON-LD for webpage type (schema.org/Event)          
      /* var jsonld = [
        '<script type="application/ld+json">',      
        '{"@context": "http://www.schema.org","@type": "Webpage",',
        '"name": "'+section.getName(language)+'",',
        '"url": "'+pageURL+'",',
        '"primaryImageOfPage": {"@type":"ImageObject", "url": "'+METAImage+'"}',
        '}',
        '</script>'].join('\n'); */
      
      var jsonld = [
        '<script type="application/ld+json">',      
        '{"@context": "http://www.schema.org","@type": "Webpage",',
          '"name": "' + META_title + '",',
          '"description": "' + META_description + '",',
          '"publisher": {',
              '"@type": "Organization",',
              '"name": "Virginia Commonwealth University",',
              '"location": {',
                  '"@type": "Place",',
                    '"address": {',
                        '"@type": "PostalAddress",',
                        '"addressLocality": "Richmond",',
                        '"addressRegion": "VA"',
                    '},',
                    '"url": "https://maps.vcu.edu/"',
                '}',
          '},',       
          '"url": "' + pageURL + '",',
          '"primaryImageOfPage": {',
              '"@type":"ImageObject",',
                '"url": "' + METAImage + '"',
          '}',
        '}',
        '</script>'].join('\n');     
  return meta + manifest + jsonld;
}