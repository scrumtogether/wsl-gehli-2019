// Scan the content and look for shortcodes

function doShortcodes(c) {
    // This will scan a piece of content searching for WSL tag shortcodes
    var rgx = /(&lt;WSL([^].*?)\/&gt;)/ig
    codes = rgx.exec(c),
        WSL = [];
    while (codes != null) {
        WSL.push(String(codes[0]));
        codes = rgx.exec(c);
    }

    // Get legacy codes with brackets '{nav_1234}'
    var rgx2 = /({events_([^].*?)})/ig
    codes2 = rgx2.exec(c);
    while (codes2 != null) {
        WSL.push(String(codes2[0]));
        codes2 = rgx2.exec(c);
    }

    // Now we have all the suspected shortcodes from the content. Now determine values and settings
    for (var i = 0; i < WSL.length; i++) {
        //document.write("<script>console.log('----------------------')</script>");
        //document.write("<script>console.log('"+WSL[i]+"')</script>");
        var tag = String(WSL[i]),
            rgxAccordion = /({accordion_([^].*?)})/i,
            rgxNavigation = /({nav_([^].*?)})/i,
            rgxSection = /({section_([^].*?)})/i,
            WSLType = /(type="([^].*?)")/i;

        var WSLtype = WSLType.exec(tag);
        var accordionTag = rgxAccordion.exec(tag);
        var navigationTag = rgxNavigation.exec(tag);
        var sectionTag = rgxSection.exec(tag);

        // If a WSL shortcode type is found, do it
        if (WSLtype != null) {
            if (WSLtype[2] == 'accordion') {
                //document.write("<script>console.log('Do WSL Accordion')</script>");
                var sID = /(sid="([^].*?)")/i,
                    cID = /(cid="([^].*?)")/i,
                    open = /(open="([^].*?)")/i,
                    sid = sID.exec(tag),
                    cid = cID.exec(tag),
                    isOpen = open.exec(tag);
                    if(sid != null){sid = sid[2]}else{sid = false}
                    if(cid != null){cid = cid[2]}else{cid = false;}
                    if(isOpen != null){isOpen = isOpen[2]}else{isOpen = false;}
                    c = doAccordions(c, sid, cid, isOpen, tag);                                  
            }
            if (WSLtype[2] == 'events') {
                //document.write("<script>console.log('Do WSL Events')</script>");
                var eventSID = /(sid="([^].*?)")/i,
                    eventShow = /(show="([^].*?)")/i,
                    sid = eventSID.exec(tag),
                    numShow = eventShow.exec(tag);
                    if(sid != null){sid = sid[2]}else{sid = false}
                    if(numShow != null){numShow = numShow[2]}else{numShow = 10;}
                    c = doEvents(c, sid, numShow, tag);
            }
            if (WSLtype[2] == 'tabs') {
                //document.write("<script>console.log('Do WSL Tabs')</script>");
                var tabSID = /(sid="([^].*?)")/i,
                    sid = tabSID.exec(tag);
                    if(sid != null){sid = sid[2]}else{sid = false}
                    c = doTabs(c, sid, tag);
            }
            if (WSLtype[2] == 'article') {
                //document.write("<script>console.log('Do WSL Article List')</script>");
                var articlesSID = /(sid="([^].*?)")/i,
                    articlesShow = /(show="([^].*?)")/i,
                    sid = articlesSID.exec(tag),
                    numShow = eventShow.exec(tag);
                    if(sid != null){sid = sid[2]}else{sid = false}
                    c = doArticles(c, sid, tag, numShow);
            }
        }
    }
    return doNavigation(c);
}
                                   
function doArticles(c, sid, tag, numShow) {
    //document.write("<script>console.log('Do Articles')</script>");
    //document.write("<script>console.log('SID: "+sid+"')</script>");
    //document.write("<script>console.log('Tg: "+tag+"')</script>");
    //document.write("<script>console.log('Content: "+c+"')</script>");
    //document.write("<script>console.log('#: "+numShow+"')</script>");
}
                                   
function doTabs(c, sid, tag) {
    //document.write("<script>console.log('Do Tabs')</script>");
    //document.write("<script>console.log("+sid+")</script>");
    //document.write("<script>console.log('"+tag+"')</script>");
    // document.write("<script>console.log('"+c+"')</script>");
   
    oCM = com.terminalfour.spring.ApplicationContextProvider.getBean(com.terminalfour.content.IContentManager);
    targetSection = TreeTraversalUtils.findSection(publishCache.getChannel(), section, sid, language);                          
    var tabs = '<div class="tab-group" role="tablist" aria-multiselectable="false" id="tab__sid-' + targetSection.getID() + '">';
    tabs = tabs + '<ul class="tabs">';
    tabContent = sortContent(targetSection.getContent());
    isActive = ' is-active',
    ariaTabState = 'aria-selected="true"';
                                   
    for (var a = 0; a < tabContent.length; a++) {
        // Get the CT and process it for use in the tabs
        aCID = tabContent[a].getContentId();
        mo = oCM.get(aCID, language);
        tabHTML = [
            '<li class="tabs__tab' + isActive + '">',
                '<div class="tabs__tab-heading">',
                    '<div class="tabs__tab-title">',
                        '<button ' + ariaTabState + ' aria-controls="tab__' + aCID + '" role="tab" onclick="toggleTabs(' + aCID + ', ' + targetSection.getID() + ',this)" class="tabs__tab-title__link">',
                            mo.getName(),
                        '</button>',
                    '</div>',
                '</div>',
            '</li>'
        ].join('\n');
        tabs = tabs +  tabHTML;
        isActive = '',
        ariaTabState = 'aria-selected="false"';
    }
    tabs = tabs + '</ul>';
    isActive = ' is-active';
    for (var a = 0; a < tabContent.length; a++) {
        // Get the CT and process it for use in the tabs
        aCID = tabContent[a].getContentId();
        if(tabContent[a].getContent().getTemplateID() == 4130){
            //doSwiperTabs(tabContent[a], targetSection)
        }
        mo = oCM.get(aCID, language);
        var sw = new java.io.StringWriter(),
            t4w = new com.terminalfour.utils.T4StreamWriter(sw);                                   
        new com.terminalfour.publish.ContentPublisher().write(t4w, dbStatement, publishCache, targetSection, mo, language, 'text/html', isPreview);
        processedContent = sw.toString();
        tabHTML = [
                '<div id="tab__' + aCID + '" role="tabpanel" class="tabs__tab-body ' + isActive + '">',
                    '<div class="tabs__tab-content">',
                        processedContent,
                    '</div>',
                '</div>'
        ].join('\n');
        tabs = tabs +  tabHTML;
        isActive = '';
    }
    tabs = tabs + '</div>';
    c = c.toString().replace('<p>' + String(tag) + '</p>', tabs);
    c = c.toString().replace(String(tag), tabs);
    return c;
}

function doSwiperTabs(swiper, thisSection){
    newContent = swiper.getContent();
    // section.addContent(newContent, section.getContentCount());
    document.write("<script>console.log('Count: " + section.getContentCount() + "')</script>");
}
                                

function doEvents(c, sid, num, tag) {
    //document.write("<script>console.log('Do events')</script>");
    //document.write("<script>console.log("+sid+")</script>");
    //document.write("<script>console.log("+num+")</script>");
    //document.write("<script>console.log('"+tag+"')</script>");
    //document.write("<script>console.log('"+c+"')</script>");

    oCM = com.terminalfour.spring.ApplicationContextProvider.getBean(com.terminalfour.content.IContentManager);
    var eventList = '<section class="l-events"><div class="l-wrapper"><div class="l-gutter">';
    var eventsContent = [];
    //tag = tag.toString().replace('&gt;', '>').replace('&lt;', '<');
    targetSection = TreeTraversalUtils.findSection(publishCache.getChannel(), section, sid, language);
    sContent = sortContent(targetSection.getContent());
    for (var a = 0; a < sContent.length; a++) {
        if (sContent[a].getContent().getTemplateID() == 4124 && new Date(oCM.get(sContent[a].getContentId(), language).get('startDate').publish()) > new Date()) {
            eventsContent.push(sContent[a]);
        }
    }
    eventsContent.sort(function(a, b) {
        return new Date(oCM.get(a.getContentId(), language).get('startDate').publish()) - new Date(oCM.get(b.getContentId(), language).get('startDate').publish());
    });
    if (Number(num) > eventsContent.length) { var showNum = eventsContent.length } else { var showNum = Number(num) }
    for (var a = 0; a < showNum; a++) {
        // Get the CT and process it for use in the accordion
        aCID = eventsContent[a].getContentId();
        mo = oCM.get(aCID, language);
        var sw = new java.io.StringWriter(),
            t4w = new com.terminalfour.utils.T4StreamWriter(sw);
        new com.terminalfour.publish.ContentPublisher().write(t4w, dbStatement, publishCache, targetSection, mo, language, 'text/shortcode', isPreview);
        processedContent = sw.toString();
        eventList = eventList + processedContent;
    }
    eventList = eventList + '</div></div></section>';
    //document.write("<script>console.log('" + tag + "')</script>");
    c = c.toString().replace('<p>' + String(tag) + '</p>', eventList);
    c = c.toString().replace(String(tag), eventList);

    return c;
}

function doArticles(c) {
    //document.write("<script>console.log('Do articles')</script>");
    oCM = com.terminalfour.spring.ApplicationContextProvider.getBean(com.terminalfour.content.IContentManager);
    var rgx = /({articles_([^].*?)})/ig,
        codes = rgx.exec(c),
        tags = [];
    while (codes != null) {
        tags.push(String(codes[0]));
        codes = rgx.exec(c);
    }
    if (tags.length == 0) {
        return c;
    } else {
        var newsContent = [];
        for (var i = 0; i < tags.length; i++) {
            sectionid = String(tags[i]).replace('{', '').replace('}', '').split('_');
            var newsList = '';
            targetSection = TreeTraversalUtils.findSection(publishCache.getChannel(), section, sectionid[1], language);
            sContent = sortContent(targetSection.getContent());
            for (var a = 0; a < sContent.length; a++) {
                if (sContent[a].getContent().getTemplateID() == 4121) {
                    newsContent.push(sContent[a]);
                }
            }
            newsContent.sort(function(a, b) {
                return new Date(oCM.get(b.getContentId(), language).get('datePublished').publish()) - new Date(oCM.get(a.getContentId(), language).get('datePublished').publish());
            });
            for (var a = 0; a < newsContent.length; a++) {
                // Get the CT and process it for use in the accordion
                aCID = newsContent[a].getContentId();
                mo = oCM.get(aCID, language);
                var sw = new java.io.StringWriter(),
                    t4w = new com.terminalfour.utils.T4StreamWriter(sw);
                new com.terminalfour.publish.ContentPublisher().write(t4w, dbStatement, publishCache, targetSection, mo, language, 'text/html', isPreview);
                processedContent = sw.toString();
                newsList = newsList + processedContent;
            }

            c = c.toString().replace('<p>' + String(tags[i]) + '</p>', newsList);
            c = c.toString().replace(String(tags[i]), newsList);
        }

    }
    return c;
}


function doNavigation(c) {
    //document.write("<script>console.log('Do navigation')</script>");
    oCM = com.terminalfour.spring.ApplicationContextProvider.getBean(com.terminalfour.content.IContentManager);
    // getShortcodes(c);
    //Find navigation shortcodes
    var rgx = /({nav_([^].*?)})/ig,
        codes = rgx.exec(c),
        tags = [];
    while (codes != null) {
        tags.push(String(codes[0]));
        codes = rgx.exec(c);
    }
    if (tags.length == 0) {
        //Just return the content 
        //return c;
        // for future development of tabbing shortcode
        return doAccordion(c);
    } else {
        // Grab the navigation object content and replace with the shortcode
        for (var i = 0; i < tags.length; i++) {
            navid = String(tags[i]).replace('{', '').replace('}', '').split('_');
            navid = navid[1];
            var navObject = T4Utils.brokerUtils.processT4Tag('<t4 type="navigation" name="Get this nav content" id="' + String(navid) + '" />');
            // Remove the <p> tag that the T4 WYSIWYG automatically adds
            c = c.replace('<p>' + String(tags[i]) + '</p>', doShortcodes(navObject));
            c = c.replace(String(tags[i]), doShortcodes(navObject));
        }
    }
    return doAccordion(c);
}

function doAccordions(c, sid, cid, open, tag) {
    //document.write("<script>console.log(" + sid + ")</script>");
    //document.write("<script>console.log(" + cid + ")</script>");
    //document.write("<script>console.log('" + open + "')</script>");
    //document.write("<script>console.log('" + tag + "')</script>");
    //return c;

    //document.write("<script>console.log('Do accordion')</script>");
    oCM = com.terminalfour.spring.ApplicationContextProvider.getBean(com.terminalfour.content.IContentManager);
    if (cid != false) {
        return doAccordionSingle(c, sid, cid, open, tag);
    }
    var accordion = '<div class="panel-group" role="tablist" aria-multiselectable="true">';
    targetSection = TreeTraversalUtils.findSection(publishCache.getChannel(), section, sid, language);
    accordionContent = sortContent(targetSection.getContent());
    for (var a = 0; a < accordionContent.length; a++) {
        // Get the CT and process it for use in the accordion
        aCID = accordionContent[a].getContentId();
        mo = oCM.get(aCID, language);
        var sw = new java.io.StringWriter(),
            t4w = new com.terminalfour.utils.T4StreamWriter(sw),
            isOpen = '';                                    
        new com.terminalfour.publish.ContentPublisher().write(t4w, dbStatement, publishCache, targetSection, mo, language, 'text/html', isPreview);
        processedContent = sw.toString();
        if(open == 'true') {isOpen = ' is-expanded'}
        panelHTML = [
            '<div class="panel-default' + isOpen + '">',
            '<div class="panel-heading" role="heading"  aria-level="2">',
            '<div class="panel-title">',
            '<button aria-expanded="false" aria-controls="accordion_' + aCID + '" href="#accordion_' + aCID + '" onclick="this.parentNode.parentNode.parentNode.classList.toggle(\'is-expanded\');this.toggleAria(\'aria-expanded\');" class="panel-title__link">',
            mo.getName(),
            '</button>',
            '</div>',
            '</div>',
            '<div id="accordion_' + aCID + '" class="panel-body collapse">',
            '<div class="panel-body__content">',
            processedContent,
            '</div>',
            '</div>',
            '</div>'
        ].join('\n');
        accordion = accordion + panelHTML;
    }
    accordion = accordion + '</div>';
    c = c.toString().replace('<p>' + String(tag) + '</p>', accordion);
    c = c.toString().replace(String(tag), accordion);
    return c;
}

function doAccordion(c) {
    //document.write("<script>console.log('Do accordion')</script>");
    oCM = com.terminalfour.spring.ApplicationContextProvider.getBean(com.terminalfour.content.IContentManager);
    var rgx = /({accordion_([^].*?)})/ig,
        codes = rgx.exec(c),
        tags = [],
        accordion = '';
    while (codes != null) {
        tags.push(String(codes[0]));
        codes = rgx.exec(c);
    }
    if (tags.length == 0) {
        return doSectionContent(c);
    } else {
        for (var i = 0; i < tags.length; i++) {
            sectionid = String(tags[i]).replace('{', '').replace('}', '').split('_');
            if (sectionid.length > 2) {
                return doAccordionSingle(c, sectionid[2], sectionid[1], tags[i]);
            }
            accordion = accordion + '<div class="panel-group" role="tablist" aria-multiselectable="true">';
            targetSection = TreeTraversalUtils.findSection(publishCache.getChannel(), section, sectionid[1], language);
            accordionContent = sortContent(targetSection.getContent());
            for (var a = 0; a < accordionContent.length; a++) {
                // Get the CT and process it for use in the accordion
                aCID = accordionContent[a].getContentId();
                mo = oCM.get(aCID, language);
                var sw = new java.io.StringWriter(),
                    t4w = new com.terminalfour.utils.T4StreamWriter(sw);
                new com.terminalfour.publish.ContentPublisher().write(t4w, dbStatement, publishCache, targetSection, mo, language, 'text/html', isPreview);
                processedContent = sw.toString();

                panelHTML = [
                    '<div class="panel-default">',
                    '<div class="panel-heading" role="heading" aria-level="2">',
                    '<div class="panel-title">',
                    '<a aria-expanded="false" aria-controls="accordion_' + aCID + '" href="#accordion_' + aCID + '" onclick="this.parentNode.parentNode.parentNode.classList.toggle(\'is-expanded\');this.toggleAria(\'aria-expanded\');" class="panel-title__link">',
                    mo.getName(),
                    '</a>',
                    '</div>',
                    '</div>',
                    '<div id="accordion_' + aCID + '" class="panel-body collapse">',
                    '<div class="panel-body__content">',
                    processedContent,
                    '</div>',
                    '</div>',
                    '</div>'
                ].join('\n');
                accordion = accordion + panelHTML;
            }
            accordion = accordion + '</div>';
            c = c.toString().replace('<p>' + String(tags[i]) + '</p>', accordion);
            c = c.toString().replace(String(tags[i]), accordion);
        }

    }
    return doSectionContent(c);
}

function doAccordionSingle(c, sectionid, cnum, open, tag) {
    //document.write("<script>console.log('Do accordion single')</script>");
    oCM = com.terminalfour.spring.ApplicationContextProvider.getBean(com.terminalfour.content.IContentManager);
    accordion = '<div class="panel-group" role="tablist" aria-multiselectable="true">';
    targetSection = TreeTraversalUtils.findSection(publishCache.getChannel(), section, sectionid, language);
    mo = oCM.get(cnum, language);
    var sw = new java.io.StringWriter(),
        t4w = new com.terminalfour.utils.T4StreamWriter(sw),
        isOpen = '';
    new com.terminalfour.publish.ContentPublisher().write(t4w, dbStatement, publishCache, targetSection, mo, language, 'text/html', isPreview);
    processedContent = sw.toString();
    if(open == 'true') {isOpen = ' is-expanded'}                                                        

    panelHTML = [
        '<div class="panel-default'+ isOpen +'">',
            '<div class="panel-heading" role="heading" aria-level="2">',
                '<div class="panel-title">',
                    '<button aria-expanded="false" aria-controls="accordion_' + cnum + '" href="#accordion_' + cnum + '" onclick="this.parentNode.parentNode.parentNode.classList.toggle(\'is-expanded\');" data-toggle="collapse" class="panel-title__link">',
                         mo.getName(),
                    '</button>',
                '</div>',
            '</div>',
            '<div id="accordion_' + cnum + '" class="panel-body collapse">',
                '<div class="panel-body__content">',
                    processedContent,
                '</div>',
            '</div>',
        '</div>'
    ].join('\n');
    accordion = accordion + panelHTML + '</div>';
    c = c.toString().replace('<p>' + String(tag) + '</p>', accordion);
    c = c.toString().replace(String(tag), accordion);
    return doSectionContent(c);
}

function doSectionContent(c) {
    //document.write("<script>console.log('Do section')</script>");
    oCM = com.terminalfour.spring.ApplicationContextProvider.getBean(com.terminalfour.content.IContentManager);
    var processedContent = '';
    var rgx = /({section_([^].*?)})/ig,
        codes = rgx.exec(c),
        tags = [];
    while (codes != null) {
        tags.push(String(codes[0]));
        codes = rgx.exec(c);
    }
    if (tags.length == 0) {
        return doArticles(c);
    } else {
        for (var i = 0; i < tags.length; i++) {
            sectionid = String(tags[i]).replace('{', '').replace('}', '').split('_');
            sectionid = sectionid[1];
            targetSection = TreeTraversalUtils.findSection(publishCache.getChannel(), section, sectionid, language);
            sectionContent = targetSection.getContent();
            for (var a = 0; a < sectionContent.length; a++) {
                // Get the CT and process it for use in the body
                var aCID = sectionContent[a].toString().split('id: ');
                aCID = aCID[1].split('- en:');
                aCID = Number(aCID[0]);
                mo = oCM.get(aCID, language);
                var sw = new java.io.StringWriter(),
                    t4w = new com.terminalfour.utils.T4StreamWriter(sw);
                new com.terminalfour.publish.ContentPublisher().write(t4w, dbStatement, publishCache, targetSection, mo, language, 'text/html', isPreview);
                processedContent = processedContent + sw.toString();
            }
            c = c.toString().replace('<p>' + String(tags[i]) + '</p>', processedContent);
            c = c.toString().replace(String(tags[i]), processedContent);
        }
        return doArticles(c);
    }
}


// End of file //
// ----------- //