importClass(com.terminalfour.content.ContentManagerImpl);

// Sorts menu items by the sequence number
function sortMenu(menu){
  menu.sort(function(a, b){
    return a.getPrintSequence() - b.getPrintSequence();
  });
  return menu;
}

function sortContent(con){
  con.sort(function(a, b){
    return a.getSequence() - b.getSequence();
  });
  return con;
}

// Get the fulltext link passing in the section and content of the item
function getFullTextLink(s, c){
  if(isPreview){
    fulltextLink = PathBuilder.getPreviewLink(chID,'en',s.getID(), c.getID(), 'text/fulltext');
  } else {
      fulltextLink = PathBuilder.getLink(dbStatement, s, publishCache, language, isPreview);
      findLink = /(<a href="([^].*?)")/i;
        URL = findLink.exec(String(fulltextLink));
        fulltextLink = URL[2] + c.getName().toLowerCase().replace(' ', '-').replace('\'', '').replace('@', '').replace('.', '').replace('&', '').replace('+', '').replace('/', '').replace(',', '').replace(':','').replace('′','').replace("’","") + '.html';
    }
    return fulltextLink;
}


// Convert T4 tags within content WYSIWYG's
// Media items, Links
function convertTags(txt, cid, sid){
  //document.write('<script>console.log("'+sid+'")</script>');
  //document.write('<script>console.log("'+cid+'")</script>');
  var find = /(<t4([^].*?)>)/ig,
      match = find.exec(txt),
      t4tags = [];
  while (match != null) {
    t4tags.push(String(match[0]));
    match = find.exec(txt);
  }
  if(t4tags.length == 0){
  document.write('<script>console.log("'+sid+'")</script>');
  document.write('<script>console.log("'+cid+'")</script>');
  document.write('<script>console.log("'+txt+'")</script>');
    return txt;
  } else {
    for (var i=0;i<t4tags.length;i++){
      var findType = /(type="([^].*?)")/i,
          type = findType.exec(t4tags[i]); 
      if(type[2] == 'sslink'){
        var findID = /(sslink_id="([^].*?)")/i,
        sslID = findID.exec(t4tags[i]);        
        link = com.terminalfour.navigation.ServerSideLinkManager.getManager().getLink(dbStatement.getConnection(), Number(sslID[2]), Number(sid), Number(cid), language);
        var sectionID = link.getToSectionID(),
            name = link.getLinkText(),
            target = TreeTraversalUtils.findSection(publishCache.getChannel(), section, sectionID, language);
        if(name == ''){
          var link = PathBuilder.getLink(dbStatement, target, publishCache, language, isPreview);
          txt = String(txt).replace(t4tags[i], link);
        } else {
          var link = PathBuilder.getLink(dbStatement, target, publishCache, language, isPreview),
              href = /(href="([^].*?)")/i,
              url = href.exec(link);                          
          txt = String(txt).replace(t4tags[i], '<a href="'+url[2]+'">'+name+'</a>');
        }        
      } else if (type[2] == 'media'){
        var mediaID = /(id="([^].*?)")/i,
            mID = mediaID.exec(t4tags[i]),
            formatter = /(formatter="([^].*?)")/i,
            f = formatter.exec(t4tags[i]),
            mediaAttributes = /(mediaattributes="([^].*?)")/i,
            mA = mediaAttributes.exec(t4tags[i]),
      attr = ''
    if(mA != null){
            attr = String(mA[2]);
        }
        if(f[2] == 'application/*'){
            // This is mostly for PDFs and adding a custom media attribute link name value
            var fMedia = contentApplicationConvert(mID[2], attr);
            txt = String(txt).replace(t4tags[i], fMedia);
        } else {
            var image = contentImageConvert(mID[2], attr);
            txt = String(txt).replace(t4tags[i], image);
        }
      }
    }// END for loop
  } // end null check
  return txt;
}

function contentImagePath(input) {
  var mediaTag = '<t4 type="media" id="' + input.getID() + '" formatter="path/*" />';
  return com.terminalfour.publish.utils.BrokerUtils.processT4Tags(dbStatement, publishCache, section, content, language, isPreview, mediaTag);
}
  
function contentImage(input) {
  var mediaTag = '<t4 type="media" id="' + input.getID() + '" formatter="image/*" />';
  return com.terminalfour.publish.utils.BrokerUtils.processT4Tags(dbStatement, publishCache, section, content, language, isPreview, mediaTag);
}

function contentImageConvert(input, attr) {
  //document.write('<script>console.log("'+attr+'")</script>');
  var mediaTag = '<t4 type="media" mediaattributes="'+attr+'" id="' + input + '" formatter="image/*" />';
  return com.terminalfour.publish.utils.BrokerUtils.processT4Tags(dbStatement, publishCache, section, content, language, isPreview, mediaTag);
}

function contentApplicationConvert(input, attr) {
  //var mediaTag = '<t4 type="media" mediaattributes="{}" id="' + input + '" formatter="application/*" />';
  var mediaTag = '<t4 type="media" mediaattributes="'+attr+'" id="' + input + '" formatter="application/*" />';
  return com.terminalfour.publish.utils.BrokerUtils.processT4Tags(dbStatement, publishCache, section, content, language, isPreview, mediaTag);
}

function contentImageTag(input) {
  var mediaTag = input;
  return com.terminalfour.publish.utils.BrokerUtils.processT4Tags(dbStatement, publishCache, section, content, language, isPreview, mediaTag);
}

function addDirectEditLinkToFooter(siteFooter, lastUpdatedDate) {
    directEditLink =  T4Utils.brokerUtils.processT4Tag('<t4 type="edit-page" action="direct-edit" text="Updated" />');
    insertPointRgx = /\{\{direct_edit_link\}\}/g;
    if (insertPointRgx.test(siteFooter)) return addUpdatedDateToFooter(siteFooter.replace(insertPointRgx, directEditLink), lastUpdatedDate);
  return siteFooter;
}

function addUpdatedDateToFooter(siteFooter, lastUpdatedDate) {
  insertPointRgx = /\{\{updated_date\}\}/g;
  if (insertPointRgx.test(siteFooter)) return siteFooter.replace(insertPointRgx, lastUpdatedDate);
  return siteFooter;
}

function contentImageAlt(input) {
  var mediaTag = '<t4 type="media" id="' + input.getID() + '" formatter="image/description"/>';
  return T4Utils.brokerUtils.processT4Tag(mediaTag);
}

function addCurrentToMenu(menu, sectionID) {
  // Add is-current to all menu items that match sectionID, this includes all parents of sub-menu items
  menu = menu.replace('{{'+sectionID+'}}', ' is-current ');
  var rgx = /({{([^].*?)}})/ig,
      codes = rgx.exec(menu);
  // Now remove all other {{sectionID}} tags to cleanup class entries
  while (codes != null) {
    menu = menu.replace(codes[1], '');
    codes = rgx.exec(menu);
  }
  var rgx = /({{([^].*?)}})/ig,
      codes = rgx.exec(menu);
  // Now remove all other {{sectionID}} tags to cleanup class entries
  while (codes != null) {
    menu = menu.replace(codes[0], '');
    codes = rgx.exec(menu);
  }
  var rgx = /({{([^].*?)}})/ig,
      codes = rgx.exec(menu);
  // Now remove all other {{sectionID}} tags to cleanup class entries
  while (codes != null) {
    menu = menu.replace(codes[0], '');
    codes = rgx.exec(menu);
  }
  menu = menu.replace('is-collapsed is-current', 'is-current is-expanded');
  return menu;
}
                                       
// Reorder content
function reOrderContent(a,b){
  var s = a.toString().split('sequence: ');
  s = s[1].split('content');
  s = Number(s[0]);
  var t = b.toString().split('sequence: ');
  t = t[1].split('content');
  t = Number(t[0]);
  return s - t;
}
                                       
function fixedEncodeURIComponent(str) {
  return encodeURIComponent(str).replace(/[!'()*]/g, function(c) {
    return '%' + c.charCodeAt(0).toString(16);
  });
}

function cleanAPIs(con, URL){
  if(!isPreview){
    var rgx = /(src="([^].*?)")/ig,
        links = rgx.exec(con),
        src = [];
    while (links != null) {
      src.push(String(links[2]));
      links = rgx.exec(con);
    }
    for (var i=0;i<src.length;i++){
      con = con.replace('src="' + src[i] + '"', 'src="'+ URL + src[i] + '"');
    }
    return con;
  }
}

// End of file //
// ----------- //